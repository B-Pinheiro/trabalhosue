#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "apt.h"
#include "symboltable.h"
#include "semant.h"


void check_declist(declist *d){

   if(d->kind == dec_empty){
      check_decl(d->u.dec_empty.dec);
   }
   else if(d->kind == dec_declist){
      check_decl(d->u.dec_declist.dec);
      check_declist(d->u.dec_declist.declist);
   }

}


type *check_decl(decl *d){

   if(d->kind == decl_id){

      char *i = d->u.id.id;
      type *ty = d->u.id.t;

      if(symtable_contains(i)){
         printf("Var is already declared!!!");
      }else{
         symtable_insert(i, ty->u.str);
      }

      return ty;

   }else if(d->kind == decl_id_assign){

      type *t = d->u.id_assign.t;
      type *ty = check_exps(d->u.id_assign.exps);

      if( t->kind != ty->kind){
         printf("Error -> the value doesn't match the type of the variable\n");
         exit(1);
      }

   }

}



type *check_exps(exps *e){
   /*
   if(e->kind == exps_id){
      if(symtable_contains(e->u.id)){
         printf("Var is al\n");
      }
   }
   else */if(e->kind == exps_id_assign){
      type *t;
      t->u.str = symtable_get(e->u.id_assign.id);
      type *ty = check_exps(e->u.id_assign.rvalue);
      if(t->kind != ty->kind){
         printf("Error -> the value doesn't match the type of the variable\n");
         exit(1);
      }else{
         return t;
      }
   }
   else if(exps_int){
      return new_type("int");
   }
   else if(exps_float){
      return new_type("float");
   }
   else if(exps_string){
      return new_type("string");
   }
   else if(exps_boolean){
      return new_type("boolean");
   }

}


void check_stmlist(stmlist *slist){

   if(slist->kind == stmlist_empty){
      check_stm(slist->u.st.s);
   }
   else if(slist->kind == stmlist_stm){
      check_stm(slist->u.st.s);
      check_stmlist(slist->u.st.slist);
   }

}


type *check_stm(stm *s){

   if (s->kind == stm_decl){
      return check_decl(s->u.d);

   } else if(s->kind == stm_exps){
      return check_exps(s->u.e);

   } else if(s->kind == stm_if){
      check_stmlist(s->u.if_empty.stlist);
      return check_exps(s->u.if_empty.e);

   } else if(s->kind == stm_if_else){
      check_stmlist(s->u.if_else.stlist1);
      check_stmlist(s->u.if_else.stlist2);

      return check_exps(s->u.if_empty.e);

   } else if(s->kind == stm_while){
      check_stmlist(s->u.whl.stlist);
      return check_exps(s->u.whl.e);

   } else if(s->kind == stm_ret){
      return check_exps(s->u.e);

   }

}


void check_arglist(arglist *a){

   if(a->kind == arglist_empty){
      check_arg(a->u.arg.ar);
   }
   else if(a->kind == arglist_arg){
      check_arg(a->u.arg.ar);
      check_arglist(a->u.arg.arlist);
   }

}


type *check_arg(arg *a){

   type *t = check_exps(a->arg1);
   if(t->kind != a->t->kind){
      printf("Erro: o argumento apresenta o tipo errado\n");
   }else{
      return t;
   }

}
