#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "huff.h"

#define filename "teste"
#define filenameout "teste.huff"
#define filenamehuff "teste.dehuff"


int main()
{

   FILE* filein;
   FILE* fileout;
   FILE* filedehuff;
   FILE* filehuff;

   node *tree;
   int table[27];
   char letters[27];
   int freq[27];
   int i;
   int table2[27];


   //Iniciar os arrays com letras e as suas frequencias
   for(i = 0; i < 26; i++){
      letters[i] = i + 97;
      freq[i] = 0;
   }
   letters[26] = 32; //add space
   freq[26] = 0;



/*
   char input[32];
   int ncount;
   //MENU
   while(1){

      for(i = 0; i < 32; i++){
         input[32] = 124;
      }

      //char input[32];
      printf("###-----Menu-----###\n");
      printf("Insira o comando que pretende usar: (a para obter ajuda)\n");

      scanf("%s", input);
      //printf("a:%s\n", input);
      if(input[0] == 97){
         printf("\n");
         printf("###----Ajuda----###\n");
         printf("huff-<ficheiro>\n");
         printf("unhuff-<ficheiro.huff>\n");
         printf("a (comandos possiveis)\n");
         printf("\n");

         //HUFF
      } else if(input[0] == 'h' && input[1] == 'u' &&input[2] == 'f' && input[3] == 'f'){

         ncount = 0;
         int ci;
         for(ci = 5; ci < 32; ci++){
            if(input[ci] > 96 && input[ci] < 123){
               printf("c:%c\n", input[ci]);
               *filename = input[ci];
               filename++;
               ncount++;
            }
         }
         *filename = '\0';
         //Voltar para o inicio
         for(ci = 0; ci < ncount; ci++){
            filename--;
         }



         //UNHUFF
      } else if(input[0] == 'u' && input[1] == 'n' && input[2] == 'h' && input[3] == 'u' && input[4] == 'f' && input[5] == 'f'){

         ncount = 0;
         int ci;
         for(ci = 7; ci < 32; ci++){
            if((input[ci] > 96 && input[ci] < 123) || input[ci] == 46){ //aceitar o ponto
               printf("c:%c\n", input[ci]);
               *filename = input[ci];
               filename++;
               ncount++;
            }
         }
         *filename = '\0';
         //Voltar para o inicio
         for(ci = 0; ci < ncount; ci++){
            filename--;
         }




      }

   }
   */

   printf("filename: %s\n", filename);


   filein = fopen(filename, "r");
   fileout = fopen(filenameout, "w+");

   //Recolher as repetições de cada caracter para um array
   int c;
   while((c = fgetc(filein)) != EOF){
      increment_frequency(freq, c);
   }

   fclose(filein);
   filein = fopen(filename, "r");


   //Criar a arvore de huffman
   build_tree(&tree, letters, freq);

   //Preecher uma tabela com as codificações para cada letra
   fill_table(table, tree, 0);

   //Mudar a ordem da codificação
   switch_code_order(table, table2);


   printf("Huffman Compression started!!\n");
   //Recebe um ficheiro .txt e codifica para .txt.huff
   compress_huffman(filein, fileout, table2);


   fclose(fileout);
   fclose(filein);
   filehuff = fopen(filenameout, "r");
   filedehuff = fopen(filenamehuff, "w+");


   printf("Huffman Decompression started!!\n");
   //Recebe um ficheiro .txt.huff e descodifica para txt.dehuff
   decompress_huffman(filehuff, filedehuff, tree);


   fclose(filehuff);
   fclose(filedehuff);

   return 0;

}
