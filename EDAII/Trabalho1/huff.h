#include <stdio.h>
#include <stdlib.h>


struct node
{
   char letter;
   int frequency;
   struct node *left, *right;
};


typedef struct node node;

void create_node(node *left, node *right, char letter, int frequency);
void increment_frequency(int freq[], char c);
int get_smaller(node *arr[], int temp_freq);
void build_tree(node **tree, char letters[], int freq[]);
void fill_table(int table[], node *tree, int code);
float calculate_gain(FILE *in, FILE* out);
void compress_huffman(FILE *in, FILE *out, int table[]);
void switch_code_order(int table[],int table2[]);
void decompress_huffman(FILE *in, FILE *out, node *tree);
