#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "huff.h"


void create_node(node *left, node *right, char letter, int frequency)
{
   node *n = malloc(sizeof(node));
   n->left = left;
   n->right = right;
   n->letter = letter;
   n->frequency = frequency;

}

void increment_frequency(int freq[], char c)
{
   if(c == 32){
      int a = freq[26];
      a++;
      freq[26] = a;
   }else{
      int a = freq[c - 97];
      a++;
      freq[c-97] = a;
   }
}

float calculate_gain(FILE *in, FILE* out)
{
   float gain = 0;
   int c;
   float insize = 0;
   float outsize = 0;

   while((c = fgetc(in)) != EOF) {
      insize = insize + 1;
   }
   while((c = fgetc(out)) != EOF) {
      outsize = outsize + 1;
   }
   if(outsize != 0){
      if(outsize > insize){
         gain = (outsize / insize);
         printf("O ficheiro gerado é maior que o original! %f vezes\n", gain);
      }else{
         gain = (outsize / insize) * 100;
         printf("Houve um ganho de %f %%\n", gain);
      }

   }

   return gain;
}



int get_smaller(node *arr[], int temp_freq)
{

   int i = 0;
   int f;

   while (i < 27 && arr[i]->frequency == -1){
      i++;
   }
   f = i;

   if( f == temp_freq){
      i++;
      while (i < 27 && arr[i]->frequency == -1){
         i++;
      }
      f = i;
   }

   for(i = 1; i < 27; i++){
      if(arr[i]->frequency == -1){
         continue;
      }
      if(i == temp_freq){
         continue;
      }
      if(arr[i]->frequency < arr[f]->frequency){
         f = i;
      }
   }

   return f;
}


void build_tree(node **tree, char letters[], int freq[])
{

   node *temp;
   node *array[27];

   int i;
   int a = 27;
   int s1, s2;

   //Criar o array com os caracteres e as suas frequencias
   for (i = 0; i < 27; i++){
      array[i] = malloc(sizeof(node));
      array[i]->frequency = freq[i];
      array[i]->letter = i;
      array[i]->left = NULL;
      array[i]->right = NULL;
   }

   while(a > 1){
      //Descobrir os dois nós com menor frequencia
      s1 = get_smaller(array, -1);
      s2 = get_smaller(array, s1);

      temp = array[s1];
      //Criar um novo nó
      array[s1] = malloc(sizeof(node));
      array[s1]->frequency = temp->frequency + array[s2]->frequency;
      array[s1]->letter = 127;
      array[s1]->left = array[s2];
      array[s1]->right = temp;
      array[s2]->frequency = -1;
      a--;

   }

   *tree = array[s1];

}


void fill_table(int table[], node *tree, int code)
{
   //Menor de 123 para selecionar todas as letras de a..z e o espaço
   if(tree->letter < 123){
      table[(int) tree->letter] = code;

   }else{
      //Avançar todos os nós para a esquerda a direita que não sejam folha da arvore
      fill_table(table, tree->left, code*10+1);
      fill_table(table, tree->right, code*10+2);
   }

}

void compress_huffman(FILE *in, FILE *out, int table[])
{

   char c, o;
   int x;
   int l = 1;

   while((c = fgetc(in)) != EOF) {
      if(c == 10){ //Ignorar o new line
         continue;
      }

      if(c==32){ //No caso de o character ser um espaço
         x = table[26];

         //Comprimento da codificação
         int tempx = x;
         for(l = 0; tempx > 0; l++){
            tempx = tempx / 10;
         }

      }else{ //Para todos os outros caracteres
         x = table[c - 97];

         //Comprimento da codificação
         int tempx = x;
         for(l = 0; tempx > 0; l++){
            tempx = tempx / 10;
         }

      }

      //Arrumar os uns e dois num array para depois colocar no ficheiro
      int number = x;
      int arrnum[l];
      int i;
      for(i = 0; i < l; ++i, number /= 10){
         arrnum[i] = number % 10;
      }

      i = 0;
      while(l > 0){
         o = arrnum[i] + 47;// if arrnum[i] = 1 then o = 0 else o = 1
         i++;
         l--;
         fputc(o, out);
      }

   }

   rewind(in);
   rewind(out);

   calculate_gain(in, out);

}

void switch_code_order(int table[],int table2[])
{
    int i;
    int n;
    int c;

    for (i = 0; i < 27; i++){
        n = table[i];
        c = 0;
        while (n > 0){
            c = c * 10 + n % 10;
            n /= 10;
        }
        table2[i] = c;
    }

}

void decompress_huffman(FILE *in, FILE *out, node *tree)
{

   node *current = tree;

   char c;

   while((c = fgetc(in)) != EOF) {

         if(c == '0'){ //Se for 0 vamos para o lado esquedo da arvore
            current = current->left;

            if(current->letter != 127){  //Ver se é folha da arvore

               if(current->letter == 26){ //Se o caracter for um espaço
                  fputc(32, out);

               }else{   //Todos os outros caracteres
                  fputc(current->letter + 97 , out);
               }

               current = tree; //Inseriu no ficheiro e volta para o root node

            }

         }else if (c == '1'){ //Se for 1 vamos para o lado direito da arvore
            current = current->right;

            if(current->letter != 127 ){  //Ver se é folha da arvore

               if(current->letter == 26){ //Se o caracter for um espaço
                  fputc(32, out);

               }else{  //Todos os outros caracteres
                  fputc(current->letter + 97 , out);
               }

               current = tree; //Inseriu no ficheiro e volta para o root node

            }

         }

   }

}
