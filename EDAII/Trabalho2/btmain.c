#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "btree.h"
#include "trie.h"
#include "aux_time.h"


void read_file_trie(trie *t, FILE* file, int max_count, char *linha, size_t len, ssize_t read){

   int counter = 0;

   if(file == NULL){
      printf("\nFicheiro Nao Encontrado");
   }

   else{

      while((read = getline(&linha, &len, file)) != -1){
         linha[strlen(linha)-1] = 0;

         if(counter < max_count){
            //printf("Linha lida: %s  counter: %d\n", linha, counter);
            trie_insert(t, linha);

         }else{
            fclose(file);
            break;
         }
         counter++;

      }

   }
}


void search_trie(trie *t, FILE* file, int max_count, char *linha, size_t len, ssize_t read){

   int counter = 0;
   int p = 0;

   if(file == NULL){
      printf("\nFicheiro Nao Encontrado");
   }

   else{

      while((read = getline(&linha, &len, file)) != -1){
         linha[strlen(linha)-1] = 0;

         if(counter < max_count){
            //printf("Linha lida: %s  counter: %d\n", linha, counter);
            int a = trie_contains(t, linha);

            if(a){
               p++;
            }
         }else{
            printf("Encontrou %d palavras\n", p);
            fclose(file);
            break;
         }
         counter++;

      }

   }

}

void read_file_btree(btree **t, FILE* file, int max_count, char *linha, size_t len, ssize_t read){

   int counter = 0;

   if(file == NULL){
      printf("\nFicheiro Nao Encontrado");
   }

   else{

      while((read = getline(&linha, &len, file)) != -1){
         linha[strlen(linha)-1] = 0;

         if(counter < max_count){
            //printf("Linha lida: %s  counter: %d\n", linha, counter);
            btree_insert(t, linha);

         }else{
            fclose(file);
            break;
         }
         counter++;

      }

   }
}


void search_btree(btree *t, FILE* file, int max_count, char *linha, size_t len, ssize_t read){

   int counter = 0;
   int p = 0;

   if(file == NULL){
      printf("\nFicheiro Nao Encontrado");
   }

   else{

      while((read = getline(&linha, &len, file)) != -1){
         linha[strlen(linha)-1] = 0;

         if(counter < max_count){
            //printf("counter: %d  p: %d\n", counter, p);
            int a = btree_search(t, linha);

            if(a){
               p ++;
            }
         }else{
            printf("Encontrou %d palavras\n", p);
            fclose(file);
            break;
         }
         counter++;

      }

   }

}

int  main()
{
   long Time;

   btree *t;
   btree_new(&t);

   trie *tr = trie_new();

   int i ;
   int max_read = 500;
   int max_search = max_read / 10;
   FILE* file_en;
   FILE* file_s;
   char* linha = NULL;
   size_t len = 0;
   ssize_t read = (ssize_t) NULL;


   printf("\n");
   printf("Trie\n");
   printf("\n");
   //Trie
   for(i = 0; i < 4; i++ ){

      //Abrir ficheiro para inserir
      file_en = fopen("words_en.txt", "r");
      //Abrir ficheiro para procurar
      file_s = fopen("words_s.txt", "r");

      //Ler palavras e inserir na trie
      start_clock();
      read_file_trie(tr, file_en, max_read, linha, len, read);
      Time = stop_clock();
      printf("Read file_en and insert %d words in trie. Time: %ld microsecs\n", max_read, Time);

      //Ler palavras e verificar se existem na trie
      start_clock();
      search_trie(tr, file_s, max_search, linha, len, read);
      Time = stop_clock();
      printf("Read file_s and search %d words in trie. Time: %ld microsecs\n", max_search, Time);

      //Remover todas as palavras da trie
      start_clock();
      trie_remove_all(tr);
      Time = stop_clock();
      printf("Deleted all words in trie. Time: %ld microsecs\n", Time);

      printf("\n");

      if(i == 0){
         max_read = max_read * 2;
         max_search = max_search * 2;
      }else if(i == 1){
         max_read = max_read * 5;
         max_search = max_search * 5;
      }else if(i == 2){
         max_read = max_read * 10;
         max_search = max_search * 10;
      }

   }

   printf("\n");
   printf("\n");
   printf("Btree\n");
   max_read = 500;
   max_search = max_read / 10;

   //Btree
   for(i = 0; i < 4; i++ ){

      printf("\n");
      btree_new(&t);
      //Abrir ficheiro para inserir
      file_en = fopen("words_en.txt", "r");
      //file_en = fopen("a.txt", "r");

      //Abrir ficheiro para procurar
      file_s = fopen("words_s.txt", "r");
      //   file_s = fopen("b.txt", "r");

      //Ler palavras e inserir na btree
      start_clock();
      read_file_btree(&t, file_en, max_read, linha, len, read);
      Time = stop_clock();
      printf("Read file_en and insert %d words in btree. Time: %ld microsecs\n", max_read, Time);


      //Ler palavras e verificar se existem na btree
      start_clock();
      search_btree(t, file_s, max_search, linha, len, read);
      Time = stop_clock();
      printf("Read file_s and search %d words in btree. Time: %ld microsecs\n", max_search, Time);

      //Remover todas as palavras da btree
      start_clock();
      btree_destroy(t);
      Time = stop_clock();
      printf("Deleted all words in btree. Time: %ld microsecs\n", Time);

      if(i == 0){
         max_read = max_read * 2;
         max_search = max_search * 2;
      }else if(i == 1){
         max_read = max_read * 5;
         max_search = max_search * 5;
      }else if(i == 2){
         max_read = max_read * 10;
         max_search = max_search * 10;
      }

   }


}
