#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "trie.h"
#include "aux_time.h"


trie *trie_new(){

   int i;

   trie *t = malloc(sizeof(trie ));

   for(i = 0; i < N; i++){
      t->next[i] = NULL;
   }

   t->size = 0;
   t->flag = 0;

   return t;
}


void trie_destroy(trie *t){

   int i;

   if(! trie_empty(t)){

      for(i = 0; i < N; i++){
         if(t->next[i] != NULL){
            trie_destroy(t->next[i]);
            t->size = t->size - 1;

            if(t->size <= 0)
               break;
         }
      }
   }

   free(t);

}

int trie_index(char ch){
   return (ch - MIN);
}

int trie_empty(trie *t){
   if(t->size <= 0)
      return 1;

   return 0;
}

void trie_insert(trie *t, char *string){

   if(*string == '\0'){
      t->flag = 1;

   }
   else{
      int i = trie_index(*string);

      if(t->next[i] == NULL){
         t->next[i] = trie_new();
         t->size = t->size + 1;
      }

      trie_insert(t->next[i], string + 1);
   }

}

int trie_contains(trie *t, char *string){

   if(! trie_empty(t)){

   int index = trie_index(*string);

   if(*string == '\0'){
      return 1;
   }
   else if(t->next[index] == NULL){
      return 0;
   }
   else{
      return trie_contains(t->next[index], string + 1);
   }
   }else{
      if(*string == '\0')
         return 1;

      return 0;
   }

}


char* trie_min(trie *t, char *string, int n){

   int i;

   if(t->flag){
      *string = 0;
      //printf("2:%s\n", string - n);
      return (string - n);
   }

   for(i = 0; i < N; i++){
      if(t->next[i] != NULL){
         //printf("%c\n", i + MIN);
         *string = i + MIN;

         trie_min(t->next[i], string + 1, n + 1);
         break;
      }

   }
   
}


void trie_print_all(trie *t, char *string, int n){

   int i;

   if(t->flag){
      *string = 0;
      printf("%s\n", string - n);
   }

   for(i = 0; i < N; i++){
      if(t->next[i] != NULL){
         //printf("%c\n", i + MIN);
         *string = i + MIN;

         trie_print_all(t->next[i], string + 1, n + 1);
      }

   }

}


void trie_remove(trie *t, char *string){

   //printf("si:%d\n", t->size);
   int index = trie_index(*string);

   if(*string == '\0'){
      //if(t->flag){
      //   printf("Removeu da trie\n");
      //}
      t->flag = 0;
   }
   else if(t->next[index] == NULL){
      printf("Não está na trie!\n");
      return;
   }
   else{

      trie_remove(t->next[index], string + 1);

      if(trie_empty(t->next[index]) && (t->next[index]->flag == 0)){
         free(t->next[index]);
         t->next[index] = NULL;
         t->size--;

      }
   }

}



void trie_remove_all(trie *t){

   char space[256];

   while(!trie_empty(t)){
      char *cs = trie_min(t, space, 0);
      //printf("1:%s\n", cs);
      trie_remove(t, cs);
   }

}
