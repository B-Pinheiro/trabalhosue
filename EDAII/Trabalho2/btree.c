#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "btree.h"
#include "aux_time.h"


void btree_new(btree **t){
   *t = malloc(sizeof(**t));
   (*t)->leaf = 1;
   (*t)->n = 0;
}

void btree_insert(btree **t, char k[] ){

   struct btree *x;

   if((*t)->n == maxkeys){
      btree_new(&x);
      x->child[0] = *t;
      *t = x;
      x->leaf = 0;
      btree_split(x, 0);

   }

   btree_insert_non_full(*t, k);

}

void btree_split(btree *t, int i){

   int j;
   btree *z = malloc(sizeof(*t));

   z->leaf = t->child[i]->leaf;
   z->n = t->child[i]->n = order - 1;

   for(j = 0; j < order - 1; j++){
      z->key[j] = t->child[i]->key[order + j];
   }
   if(! z->leaf){
      for(j = 0; j < order; j++){
         z->child[j] = t->child[i]->child[order + j];
      }
   }

   for(j = t->n; j > i; j-- ){
      t->key[j] = t->key[j - 1];
   }
   for (j = t->n; j > i; j--){
      t->child[j + 1] = t->child[j];
   }

   t->key[i] = t->child[i]->key[order - 1];
   t->child[i+1] = z;
   t->n = t->n+1;

}

void btree_insert_non_full(btree *t, char k[]){

   int i, j;

   if(t->leaf){
      for(i = 0; (i < t->n) && (strcmp(t->key[i], k) < 0); i++);
      for(j = t->n; j > i; j--) t->key[j] = t->key[j-1];

      t->key[i] = strdup(k);
      //printf("%s\n", k);
      t->n = t->n+1;

   }else{
      for(i = 0; (i < t->n) && (strcmp(t->key[i], k) < 0); i++);

      if(t->child[i]->n == maxkeys){
         btree_split(t, i);
         btree_insert_non_full(t, k);

      }else{
         btree_insert_non_full(t->child[i], k);

      }
   }
}


int btree_search(btree *t, char k[]){

   int i;

   for(i = 0; i < t->n; i++ ){
      if(strcmp(t->key[i], k) == 0){
         return 1;

      }else if(strcmp(t->key[i], k) > 0){
         break;
      }

   }

   if(t->leaf){
      return 0;

   }else{
      return ( btree_search(t->child[i], k ) );
   }


}



void btree_show(btree *t, int b)
{
   int i, j;

   if(! t->leaf){
      for(i = t->n; i >= 0; i--){
         btree_show(t->child[i], b+3 );
      }
   }

   for (i = t->n - 1; i >= 0; i--){
      for(j = 0; j < b; j++){
         printf(" ");
      }
      printf("|%s|\n", t->key[i]);

   }
   printf("\n");

}


char *btree_predecessor(btree *t, char k[]){

   int i = 0;

   while((i < t->n) && (strcmp(t->key[i], k) < 0) ){
      i++;
   }

   if(i == t->n)
      return 0;

   if(strcmp(t-> key[i], k) == 0)
      return t->key[i-1];

   if(! t->leaf)
      return ( btree_predecessor(t->child[i], k ) );

   return 0;

}

char *btree_sucessor(btree *t, char k[]){

   int i = 0;

   while((i < t->n) && (strcmp(t->key[i], k) < 0) ){
      i++;
   }

   if(i == t->n)
      return NULL;

   if(strcmp(t-> key[i], k) == 0)
      return t->key[i+1];

   if(! t->leaf)
      btree_sucessor(t->child[i], k);

   return 0;

}


void btree_destroy(btree *t){

   int i;

   if(!t->leaf){
      for(i = 0; i < t->n+1; i++ ){
         btree_destroy(t->child[i]);
      }
   }
   free(t);

}
