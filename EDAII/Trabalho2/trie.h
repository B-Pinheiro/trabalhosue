#define MIN 'a'
#define MAX 'z'
#define N (MAX - MIN + 1)


//typedef

typedef struct trie trie;

//
struct trie{
  int flag;
  int size;
  trie *next[N];
};

trie *trie_new();
void trie_destroy(trie *t);
int trie_index(char string);
int trie_empty(trie *t);
void trie_insert(trie *t, char *string);
int trie_contains(trie *t, char *string);
char* trie_min(trie *t, char *string, int n);
void trie_print_all(trie *t, char *string, int n);
void trie_remove(trie *t, char *string);
void trie_remove_all(trie *t);
void trie_get_all(trie *t, char *string, int n);
