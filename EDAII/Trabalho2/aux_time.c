#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include "aux_time.h"

static struct timeval tv1, tv2;

long timediff(struct timeval t2, struct timeval t1)
{
    return (t2.tv_usec + 1000000 * t2.tv_sec) - (t1.tv_usec + 1000000 * t1.tv_sec);
}

void start_clock()
{
  gettimeofday(&tv1, NULL);
}

long stop_clock()
{
  gettimeofday(&tv2, NULL);

  return timediff(tv2, tv1);
}
