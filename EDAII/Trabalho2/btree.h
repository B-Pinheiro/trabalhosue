#define order 3
#define maxchildren 2*order
#define maxkeys 2*order-1

//typedef

typedef struct btree btree;

//struct

struct btree {
  int leaf;
  int n;
  char *key[maxkeys];
  struct btree *child[maxchildren];
};


//

void btree_new(btree **t);
void btree_insert(btree **t, char k[]);
void btree_insert_non_full(btree *t, char k[]);
void btree_split(btree *t, int i);
void btree_show(btree *t, int b);
int btree_search(btree *t, char k[]);
char *btree_predecessor(btree *t, char k[]);
char *btree_sucessor(btree *t, char k[]);
void btree_destroy(btree *t);
