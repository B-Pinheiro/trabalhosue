
function rect(width, height, depth, c){


	var boxGeometry = new THREE.BoxGeometry( width, height, depth);

	var boxMaterial = new THREE.MeshBasicMaterial( { color: c } );

	return (new THREE.Mesh( boxGeometry, boxMaterial));

}

function draw_circle(radius, c){
    
    var geometry = new THREE.CircleGeometry( radius , 20);
    var material = new THREE.MeshBasicMaterial( { color: c } );
    return (new THREE.Mesh( geometry, material ));
}

function cube(width, height, depth, c){

	var geometry = new THREE.BoxGeometry( width, height, depth );
	var material = new THREE.MeshBasicMaterial( { color: c } );

	return (new THREE.Mesh( geometry, material ));

}

function draw_cylinder(radiusTop, radiusBottom, height, c){

	var geometry = new THREE.CylinderGeometry( radiusTop, radiusBottom, height);
	var material = new THREE.MeshBasicMaterial( {color: c } );
	
	return (new THREE.Mesh( geometry, material ));

}

function draw_plane(width, height, c){
  
    var geometry = new THREE.PlaneBufferGeometry( width, height, 32 );
    var material = new THREE.MeshBasicMaterial( {color: c, side: THREE.DoubleSide} );
    return (new THREE.Mesh( geometry, material ));
    
}

function draw_tube(){
    
    var CustomSinCurve = THREE.Curve.create(
        function ( scale ) { //custom curve constructor
            this.scale = (scale === undefined) ? 1 : scale;
        },

        function ( t ) { //getPoint: t is between 0-1
            var tx = t * 4 - 1.5,
            ty = 1.4 * Math.sin( -Math.PI * t),
            tz = 0;

        return new THREE.Vector3(tx, ty, tz).multiplyScalar(this.scale);
        }
    );

    var path = new CustomSinCurve( 17.7 );
    
    var geometry = new THREE.TubeGeometry(
        path,  //path
        20,    //segments   
        0.3,     //radius
        8,     //radiusSegments
        false  //closed
    );
    
    return geometry;

}


function draw_tube2(a, b, c, s, r){
    
    var CustomCosCurve = THREE.Curve.create(
        function ( scale ) { //custom curve constructor
            this.scale = (scale === undefined) ? 1 : scale;
        },

        function ( t ) { //getPoint: t is between 0-1
            var tx = t * b - 1.5,
            ty = a * Math.sin( Math.PI * t * c),
            tz = 0;

        return new THREE.Vector3(tx, ty, tz).multiplyScalar(this.scale);
        }
    );

    var path = new CustomCosCurve( s );
    
    var geometry = new THREE.TubeGeometry(
        path,  //path
        20,    //segments   
        r,     //radius
        8,     //radiusSegments
        false  //closed
    );
    
    return geometry;

}

function draw_tri(c){
    
    var triShape = new THREE.Shape();
    
    triShape.moveTo(0, 0);
    triShape.lineTo(0, 1);
    triShape.lineTo(1, 1);
    triShape.lineTo(0,0);
    
    var triGeom = new THREE.ShapeGeometry( triShape );

    return (new THREE.Mesh( triGeom, new THREE.MeshBasicMaterial( { color: c, wireframe: true} ) ));
    
}

function draw_base(scene){
    
    var color = 0xde003d;
    var grey = 0xb8b4a9;
    //Base 
	var base1 = rect(130, 0.5, 7.5, color);
    
    base1.position.y = 3;
    
    //Base 2
    var base2 = rect(130, 0.1, 7.5, color);
    
    base2.position.y = 1.5;
    
    
    //Base 3
    var base31 = rect(1, 11.5, 1, grey);
    var base32 = rect(1, 11.5, 1, grey);
    var base33 = rect(1, 11.5, 1, grey);
    var base34 = rect(1, 11.5, 1, grey);

    base31.position.set(49, -0.8, -3);
    base32.position.set(49, -0.8, 3);
    base33.position.set(58, -0.8, 3);
    base34.position.set(58, -0.8, -3);   
 
    
    
    for(var i = 0; i < 70; i++){
        var tri1 = draw_tri(color);
        var tri2 = draw_tri(color);
        
        var tri3 = draw_tri(color);
        var tri4 = draw_tri(color);
        
        tri1.position.set(-i*0.925, 2.7, 3.6);
        tri1.rotation.set(Math.PI, Math.PI, -Math.PI/4);
        
        tri2.position.set(i*0.925, 2.7, 3.6);
        tri2.rotation.set(Math.PI, Math.PI, -Math.PI/4);
        //tri2.rotation.set(Math.PI, Math.PI, -2 * Math.PI);
        
        tri3.position.set(-i*0.935, 1.7, 3.6);
        tri3.rotation.set(0, 0, -Math.PI/4);
        
        tri4.position.set(i*0.9, 1.7, 3.6);
        tri4.rotation.set(0, 0, -Math.PI/4);
        
        scene.add(tri1);
        scene.add(tri2);
        scene.add(tri3);
        scene.add(tri4);
        
    }
    
    for(var i = 0; i < 70; i++){
        
        var tri5 = draw_tri(color);
        var tri6 = draw_tri(color);
        
        var tri7 = draw_tri(color);
        var tri8 = draw_tri(color);
        
        tri5.position.set(-i*0.925, 2.7, -3.6);
        tri5.rotation.set(Math.PI, Math.PI, -Math.PI/4);
        
        tri6.position.set(i*0.925, 2.7, -3.6);
        tri6.rotation.set(Math.PI, Math.PI, -Math.PI/4);
        //tri2.rotation.set(Math.PI, Math.PI, -2 * Math.PI);
        
        tri7.position.set(-i*0.935, 1.7, -3.6);
        tri7.rotation.set(0, 0, -Math.PI/4);
        
        tri8.position.set(i*0.9, 1.7, -3.6);
        tri8.rotation.set(0, 0, -Math.PI/4);
        
        scene.add(tri5);
        scene.add(tri6);
        scene.add(tri7);
        scene.add(tri8);
    
    }
    
    //Base4
    var base4 = rect(130, 0.1, 5.5, 0x2f3337);
    base4.position.set(0,3.21,0);
    
    
    //Base 5
    var base5 = draw_plane(10.5, 7.5, 0xb8b4a9);
    
    base5.position.set(53.5,-6.7,0);
    base5.rotation.set(Math.PI/2, Math.PI, Math.PI);
    
    //Add to Scene
    scene.add(base1);
    scene.add(base2);
    scene.add(base31);
    scene.add(base32);
    scene.add(base33);
    scene.add(base34);
    scene.add(base4);
    scene.add(base5);
    
}


function draw_pilar_cima(scene){
    
    var color = 0xde003d;
    
    //Pilar cima 1
	var pilar_cima10 = cube(1, 7, 1, color);
    var pilar_cima11 = cube(0.85, 6.5, 0.85, color);
    var pilar_cima12 = cube(0.65, 5.5, 0.65, color);
    var pilar_cima13 = cube(0.5, 5, 0.5, color);
    
    pilar_cima10.position.set(-45, 6.7, 3);

    pilar_cima11.position.set(-45, 14.4, 3);

    pilar_cima12.position.set(-45, 21.2, 3);

    pilar_cima13.position.set(-45, 26.5, 3);  
    
    
    //Pilar cima 2
	var pilar_cima20 = cube(1, 7, 1, color);
    var pilar_cima21 = cube(0.85, 6.5, 0.85, color);
    var pilar_cima22 = cube(0.65, 5, 0.65, color);
    var pilar_cima23 = cube(0.5, 5.5, 0.5, color);
    
    
    pilar_cima20.position.set(26, 6.7, 3);

    pilar_cima21.position.set(26, 14.4, 3);

    pilar_cima22.position.set(26, 21.2, 3);

    pilar_cima23.position.set(26, 26.5, 3);
     
    
    //Pilar cima 3
    var pilar_cima30 = cube(1, 7, 1, color);
    var pilar_cima31 = cube(0.85, 6.5, 0.85, color);
    var pilar_cima32 = cube(0.65, 5, 0.65, color);
    var pilar_cima33 = cube(0.5, 5.5, 0.5, color);
    
    pilar_cima30.position.set(26, 6.7, -3);

    pilar_cima31.position.set(26, 14.4, -3);

    pilar_cima32.position.set(26, 21.2, -3);

    pilar_cima33.position.set(26, 26.5, -3);
    
    
    //Pilar cima 4
	var pilar_cima40 = cube(1, 7, 1, color);
    var pilar_cima41 = cube(0.85, 6.5, 0.85, color);
    var pilar_cima42 = cube(0.65, 5, 0.65, color);
    var pilar_cima43 = cube(0.5, 5.5, 0.5, color);
    
    pilar_cima40.position.set(-45, 6.7, -3);

    pilar_cima41.position.set(-45, 14.4, -3);

    pilar_cima42.position.set(-45, 21.2, -3);

    pilar_cima43.position.set(-45, 26.5, -3);
    
    
    draw_joins(scene);
    
    
    //Add to Scene
    scene.add(pilar_cima10);
    scene.add(pilar_cima11);
    scene.add(pilar_cima12);
    scene.add(pilar_cima13);
    
	
    scene.add(pilar_cima20);
    scene.add(pilar_cima21);
    scene.add(pilar_cima22);
    scene.add(pilar_cima23);
		

    scene.add(pilar_cima30);
    scene.add(pilar_cima31);
    scene.add(pilar_cima32);
    scene.add(pilar_cima33);
    
    
    scene.add(pilar_cima40);
    scene.add(pilar_cima41);
    scene.add(pilar_cima42);
    scene.add(pilar_cima43);
    
}


function draw_pilar_baixo(scene){
    
    var color = 0xde003d;
    var grey = 0xb8b4a9;
    
    //Pilar baixo 1
	var pilar_baixo10 = cube(1, 7, 1, color);
    var pilar_baixo11 = cube(1, 7, 1, color);
    var pilar_baixo12 = cube(6.5, 1, 1, 0x8e7671);
    
    pilar_baixo10.position.set(-45, -2.3, -2.5);

    pilar_baixo11.position.set(-45, -2.3, 2.5);
    
    pilar_baixo12.position.set(-45, -7.3, 0);
    pilar_baixo12.rotation.set(Math.PI, Math.PI/2, Math.PI);
    
    //Pilar baixo 2
	var pilar_baixo20 = cube(1, 7, 1, color);
    var pilar_baixo21 = cube(1, 7, 1, color);
    var pilar_baixo22 = cube(6.5, 1, 1, 0x8e7671);
    
    pilar_baixo20.position.set(26, -2.3, -2.5);

    pilar_baixo21.position.set(26, -2.3, 2.5);
    
    pilar_baixo22.position.set(26, -7.3, 0);
    pilar_baixo22.rotation.set(Math.PI, Math.PI/2, Math.PI);
    
    
    
    //Circle Base
    var circle1 = draw_circle(5, grey);
    var circle2 = draw_circle(5, grey);
    var circle11 = draw_circle(5, grey);
    var circle22 = draw_circle(5, grey);    
    
    circle1.rotation.set(Math.PI/2, Math.PI, Math.PI);
    circle1.position.set(26, -7.82, 0);
    
    circle2.rotation.set(Math.PI/2, Math.PI, Math.PI);
    circle2.position.set(-44.75, -7.82, 0);
    
    circle11.rotation.set(-Math.PI/2, Math.PI, Math.PI);
    circle11.position.set(26, -7.82, 0);
    
    circle22.rotation.set(-Math.PI/2, Math.PI, Math.PI);
    circle22.position.set(-44.75, -7.82, 0);
    
    //Add to Scene
    scene.add(pilar_baixo10);
    scene.add(pilar_baixo11);
    scene.add(pilar_baixo12);
    
    scene.add(pilar_baixo20);
    scene.add(pilar_baixo21);
    scene.add(pilar_baixo22);
    
    scene.add(circle1);
    scene.add(circle2);
    scene.add(circle11);
    scene.add(circle22);
    
}

function draw_circle_pilar(scene, color){
    
    //Pilar 1    
    var circle1 = draw_circle(0.4, color);
    var circle2 = draw_circle(0.4, color);
    var circle11 = draw_circle(0.4, color);
    var circle22 = draw_circle(0.4, color);
    
    var circle5 = draw_circle(0.55, color);
    var circle6 = draw_circle(0.55, color);
    var circle55 = draw_circle(0.55, color);
    var circle66 = draw_circle(0.55, color);
    
     var circle7 = draw_circle(0.55, color);
    var circle8 = draw_circle(0.55, color);
    var circle77 = draw_circle(0.55, color);
    var circle88 = draw_circle(0.55, color);
    
    
    circle1.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle1.position.set(-45, -3.85, 0);
    
    circle2.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle2.position.set(-45, -0.4, 0);
    
    circle11.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle11.position.set(-45, -3.85, 0);
    
    circle22.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle22.position.set(-45, -0.4, 0);
    
    circle5.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle5.position.set(-45, -2.15, -1.8);
    
    circle6.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle6.position.set(-45, -2.15, 1.8);
    
    circle55.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle55.position.set(-45, -2.15, -1.8);
    
    circle66.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle66.position.set(-45, -2.15, 1.8);
    
    circle7.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle7.position.set(-45, -5.6, -1.8);
    
    circle8.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle8.position.set(-45, -5.6, 1.8);
    
    circle77.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle77.position.set(-45, -5.6, -1.8);
    
    circle88.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle88.position.set(-45, -5.6, 1.8);    
    
    
    //Pilar 2
    var circle3 = draw_circle(0.4, color);
    var circle4 = draw_circle(0.4, color);
    var circle33 = draw_circle(0.4, color);
    var circle44 = draw_circle(0.4, color);
    
    var circle9 = draw_circle(0.55, color);
    var circle10 = draw_circle(0.55, color);
    var circle99 = draw_circle(0.55, color);
    var circle1010 = draw_circle(0.55, color);
    
    var circle0 = draw_circle(0.55, color);
    var circle01 = draw_circle(0.55, color);
    var circle00 = draw_circle(0.55, color);
    var circle0101 = draw_circle(0.55, color);
    
    
    circle3.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle3.position.set(26, -3.85, 0);
    
    circle4.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle4.position.set(26, -0.4, 0);
    
    circle33.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle33.position.set(26, -3.85, 0);
    
    circle44.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle44.position.set(26, -0.4, 0);
    
    circle9.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle9.position.set(26, -2.15, -1.8);
    
    circle10.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle10.position.set(26, -2.15, 1.8);
    
    circle99.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle99.position.set(26, -2.15, -1.8);
    
    circle1010.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle1010.position.set(26, -2.15, 1.8);
    
    circle0.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle0.position.set(26, -5.6, -1.8);
    
    circle01.rotation.set(Math.PI, -Math.PI/2, Math.PI);
    circle01.position.set(26, -5.6, 1.8);
    
    circle00.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle00.position.set(26, -5.6, -1.8);
    
    circle0101.rotation.set(Math.PI, Math.PI/2, Math.PI);
    circle0101.position.set(26, -5.6, 1.8);  
    
    scene.add(circle1);
    scene.add(circle2);
    scene.add(circle11);
    scene.add(circle22);
    
    scene.add(circle3);
    scene.add(circle4);
    scene.add(circle33);
    scene.add(circle44);
    
    scene.add(circle5);
    scene.add(circle6);
    scene.add(circle55);
    scene.add(circle66);
    
    scene.add(circle7);
    scene.add(circle8);
    scene.add(circle77);
    scene.add(circle88);
    
    scene.add(circle9);
    scene.add(circle10);
    scene.add(circle99);
    scene.add(circle1010);
     
    scene.add(circle0);
    scene.add(circle01);
    scene.add(circle00);
    scene.add(circle0101);
    
}


function draw_rect_pilar(scene){
    
    var color = 0xde003d;    
    
    //PILAR CIMA
    
    //Junção do pilar de cima 1
    var pilar_cima_1_rect1 = rect(0.5, 2, 6, color);
    var pilar_cima_1_rect2 = rect(0.5, 2, 6, color);
    var pilar_cima_1_rect3 = rect(0.5, 2, 6, color);
    var pilar_cima_1_rect4 = rect(0.5, 2, 6, color);
    
   
    pilar_cima_1_rect1.position.set(-45, 9, 0);
    pilar_cima_1_rect2.position.set(-45, 16.5, 0);
    pilar_cima_1_rect3.position.set(-45, 22.5, 0);
    pilar_cima_1_rect4.position.set(-45, 28, 0);
    
    
    //Junção do pilar de cima 2
    var pilar_cima_2_rect1 = rect(0.5, 2, 6, color);
    var pilar_cima_2_rect2 = rect(0.5, 2, 6, color);
    var pilar_cima_2_rect3 = rect(0.5, 2, 6, color);
    var pilar_cima_2_rect4 = rect(0.5, 2, 6, color);
    
    
    pilar_cima_2_rect1.position.set(26, 9, 0);
    pilar_cima_2_rect2.position.set(26, 16.5, 0);
    pilar_cima_2_rect3.position.set(26, 22.5, 0);
    pilar_cima_2_rect4.position.set(26, 28, 0);
    
    
    
    //PILAR BAIXO
    
    //Junção do pilar de baixo 1
    var pilar_baixo_1_rect1 = rect(0.5, 0.3, 4, color);
    var pilar_baixo_1_rect2 = rect(0.5, 0.3, 4, color);
    
    var pilar_baixo_1_rect3 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_1_rect4 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_1_rect5 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_1_rect6 = rect(0.5, 0.3, 2.2, color);
    
    var pilar_baixo_1_rect7 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_1_rect8 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_1_rect9 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_1_rect10 = rect(0.5, 0.3, 2.2, color);
   
    pilar_baixo_1_rect1.position.set(-45, -2.15, 0);
    pilar_baixo_1_rect2.position.set(-45, -5.58, 0);
    
    pilar_baixo_1_rect3.position.set(-45, 0.4, -1);
    pilar_baixo_1_rect4.position.set(-45, 0.4, 1);
    pilar_baixo_1_rect5.position.set(-45, -1.2, 1);
    pilar_baixo_1_rect6.position.set(-45, -1.2, -1);
    
    pilar_baixo_1_rect7.position.set(-45, -3.1, -1);
    pilar_baixo_1_rect8.position.set(-45, -3.1, 1);
    pilar_baixo_1_rect9.position.set(-45, -4.5, 1);
    pilar_baixo_1_rect10.position.set(-45, -4.5, -1);
    
    
    pilar_baixo_1_rect3.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_1_rect4.rotation.set(-Math.PI/4, 0, Math.PI);
    pilar_baixo_1_rect5.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_1_rect6.rotation.set(-Math.PI/4, 0, Math.PI);
    
    pilar_baixo_1_rect7.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_1_rect8.rotation.set(-Math.PI/4, 0, Math.PI);
    pilar_baixo_1_rect9.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_1_rect10.rotation.set(-Math.PI/4, 0, Math.PI);
    
    
    //Junção do pilar 2
    var pilar_baixo_2_rect1 = rect(0.5, 0.3, 4, color);
    var pilar_baixo_2_rect2 = rect(0.5, 0.3, 4, color);
    
    var pilar_baixo_2_rect3 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_2_rect4 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_2_rect5 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_2_rect6 = rect(0.5, 0.3, 2.2, color);
    
    var pilar_baixo_2_rect7 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_2_rect8 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_2_rect9 = rect(0.5, 0.3, 2.2, color);
    var pilar_baixo_2_rect10 = rect(0.5, 0.3, 2.2, color);
    
    pilar_baixo_2_rect1.position.set(26, -2.15, 0);
    pilar_baixo_2_rect2.position.set(26, -5.58, 0);
    
    pilar_baixo_2_rect3.position.set(26, 0.4, -1);
    pilar_baixo_2_rect4.position.set(26, 0.4, 1);
    pilar_baixo_2_rect5.position.set(26, -1.2, 1);
    pilar_baixo_2_rect6.position.set(26, -1.2, -1);
    
    pilar_baixo_2_rect7.position.set(26, -3.1, -1);
    pilar_baixo_2_rect8.position.set(26, -3.1, 1);
    pilar_baixo_2_rect9.position.set(26, -4.5, 1);
    pilar_baixo_2_rect10.position.set(26, -4.5, -1);
    
    
    pilar_baixo_2_rect3.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_2_rect4.rotation.set(-Math.PI/4, 0, Math.PI);
    pilar_baixo_2_rect5.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_2_rect6.rotation.set(-Math.PI/4, 0, Math.PI);
    
    pilar_baixo_2_rect7.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_2_rect8.rotation.set(-Math.PI/4, 0, Math.PI);
    pilar_baixo_2_rect9.rotation.set(Math.PI/4, 0, Math.PI);
    pilar_baixo_2_rect10.rotation.set(-Math.PI/4, 0, Math.PI);
    
    
    //Add circles
    draw_circle_pilar(scene, color);
    
    
    //Add to Scene
    scene.add(pilar_cima_1_rect1);
    scene.add(pilar_cima_1_rect2);
    scene.add(pilar_cima_1_rect3);
    scene.add(pilar_cima_1_rect4);
    
    scene.add(pilar_cima_2_rect1);
    scene.add(pilar_cima_2_rect2);
    scene.add(pilar_cima_2_rect3);
    scene.add(pilar_cima_2_rect4);
    
    scene.add(pilar_baixo_1_rect1);
    scene.add(pilar_baixo_1_rect2);
    scene.add(pilar_baixo_1_rect3);
    scene.add(pilar_baixo_1_rect4);
    scene.add(pilar_baixo_1_rect5);
    scene.add(pilar_baixo_1_rect6);
    scene.add(pilar_baixo_1_rect7);
    scene.add(pilar_baixo_1_rect8);
    scene.add(pilar_baixo_1_rect9);
    scene.add(pilar_baixo_1_rect10);
    
    scene.add(pilar_baixo_2_rect1);
    scene.add(pilar_baixo_2_rect2);
    scene.add(pilar_baixo_2_rect3);
    scene.add(pilar_baixo_2_rect4);
    scene.add(pilar_baixo_2_rect5);
    scene.add(pilar_baixo_2_rect6);
    scene.add(pilar_baixo_2_rect7);
    scene.add(pilar_baixo_2_rect8);
    scene.add(pilar_baixo_2_rect9);
    scene.add(pilar_baixo_2_rect10);
    

}


function draw_joins(scene){
    
     var color = 0xd33c41;   
    
    //Cylinders
    //Pilar Cima
    var cylinder1 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder2 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder3 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder4 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder5 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder6 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder7 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder8 = draw_cylinder(0.7, 0.7, 1, color);
    
    //Pilar Baixo
    var cylinder9 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder10 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder11 = draw_cylinder(0.7, 0.7, 1, color);
    var cylinder12 = draw_cylinder(0.7, 0.7, 1, color);
    
    
    cylinder1.position.set(-45, 10.7, -3);
    cylinder2.position.set(-45, 10.7, 3);
    cylinder3.position.set(-45, 18.2, -3);
    cylinder4.position.set(-45, 18.2, 3);
    cylinder5.position.set(26, 18.2, -3);
    cylinder6.position.set(26, 18.2, 3);
    cylinder7.position.set(26, 10.7, -3);
    cylinder8.position.set(26, 10.7, 3);
    
    cylinder9.position.set(-45, -6.3, -2.5);
    cylinder10.position.set(-45, -6.3, 2.5);
    cylinder11.position.set(26, -6.3, -2.5);
    cylinder12.position.set(26, -6.3, 2.5);
    
    
    //Add to Scene
    scene.add(cylinder1);
    scene.add(cylinder2);
    scene.add(cylinder3);
    scene.add(cylinder4);
    scene.add(cylinder5);
    scene.add(cylinder6);
    scene.add(cylinder7);
    scene.add(cylinder8);
    
    scene.add(cylinder9);
    scene.add(cylinder10);
    scene.add(cylinder11);
    scene.add(cylinder12);
    

}


function draw_cables(scene){
    
    var c = 0xde0010;
    
    //Tubes
    var tube1 = draw_tube();
    
    var tube2 = draw_tube2(-1, 0.9, 0.5, 25, 0.3);

    var tube3 = draw_tube2(1, -0.6, -0.5, 25, 0.3);
    
    var material = new THREE.MeshBasicMaterial( { color: c } );
    
    //Cable 1
    var cable1 = new THREE.Mesh( tube1, material);
    
    cable1.position.set(-18.3, 29.5, 2.8);
    
    
    //Cable 2
    var cable2 = new THREE.Mesh( tube1, material);
    
    cable2.position.set(-18.3, 29.5, -2.8);
    
    
    //Cable 3
    var cable3 = new THREE.Mesh( tube2, material);
    
    cable3.position.set(63.7, 29.7, 2.8);
    
    
    //Cable 4
    var cable4 = new THREE.Mesh( tube2, material);
    
    cable4.position.set(63.7, 29.7, -2.8);
    
    
    //Cable 5
    var cable5 = new THREE.Mesh( tube3, material);
    
    cable5.position.set(-7.5, 29.8, 2.8);
    
    //Cable 6
    var cable6 = new THREE.Mesh( tube3, material);
    
    cable6.position.set(-7.5, 29.8, -2.8);
    
    
    //Add to Scene
    scene.add( cable1 );
    scene.add( cable2 );
    scene.add( cable3 );
    scene.add( cable4 );
    scene.add( cable5 );
    scene.add( cable6 );
    
        
    var cables1 = new Array(15);
    var cables2 = new Array(15);
    
    var counter = 26;
    var d = 29;
    for(var i = 15; i > 0; i--){
        
        counter-=1.5;
        d -= 1.5;

        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(-20 + (d-i) , d, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(-20 + (d-i) , d, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 26;
    d = 29;
    for(var i = 0; i < 15; i++){

        counter -=1.5;
        d -= 1.5;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(10 + (d + counter - i) , d, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(10 + (d + counter - i) , d, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 5;
    d = 2;
    for(var i = 0; i < 5; i++){
        
        counter -= 0.75;
        d -= 0.75;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(1 + (d + counter - i) , d + 6, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(1 + (d + counter - i) , d + 6, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 5;
    d = 2;
    for(var i = 5; i > 0; i--){
        
        counter -= 0.75;
        d -= 0.75;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(-8.5 + (d- i) , d + 6, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(-8.5 + (d - i) , d + 6, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }

    for(var i = 0; i < 6; i++){
        
        var tube4 = draw_tube2(-1, 0, 1, 2, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(-8.5+ i, 5, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(-8.5+ i, 5, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 27;
    d = 27;
    for(var i = 8; i > 0; i--){
        
        counter-=2.25;
        d -= 2.65;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(46.5 + (d-i) , d + 5, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(46.5 + (d-i) , d + 5, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 7;
    d = 6;
    for(var i = 8; i > 0; i--){
        
        counter-=0.5;
        d -= 0.7;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(51.8 + (d-i) , d + 4.25, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(51.8 + (d-i) , d + 4.25, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 24;
    d = 20;
    for(var i = 0; i < 10; i++){

        counter -=1.5;
        d -= 1.5;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(-53 + (d + counter) , d + 7, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(-53 + (d + counter ) , d + 7, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    
    counter = 8;
    d = 7;
    for(var i = 0; i < 8; i++){
        
        counter-=0.75;
        d -= 0.75;
        
        var tube4 = draw_tube2(-1, 0, 1, counter, 0.1);
        
        cables1[i] = new THREE.Mesh( tube4, material);
        cables1[i].position.set(-48.9 + (d -i) , d + 4.25, 2.8);
        
        cables2[i] = new THREE.Mesh( tube4, material);
        cables2[i].position.set(-48.9 + (d - i) , d + 4.25, -2.8);
        
        scene.add(cables1[i]);
        scene.add(cables2[i]);
    }
    

}


function init() {
    
    //INIT VARS
	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    
    var controls;
    
    //CAMERA POSITION
    //camera.position.set(0, 100, 0);
    //camera.position.set(100, 10, -30);
    camera.position.set(0,30,80);
    //camera.position.set(-80,50,10);    
    //camera.lookAt( scene.position );
    
    
    //RENDERER
	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);



    
    //var text = TextGeometry("Golden Gate Bridge", helvetiker, 12);

    
    //Draw
    draw_base(scene);
    
    draw_pilar_cima(scene);
        
    draw_pilar_baixo(scene);

    draw_rect_pilar(scene);
    
    draw_cables(scene);
    
    //scene.add(text);


//	camera.position.z = 15;

	
	//camera.lookAt( scene.position );

	//controls = new THREE.OrbitControls(camera);

	//Lights
/*
	var ambient_light = new THREE.AmbientLight(0xFFFFFF, 2, 100);
	ambient_light.z = 10;
	scene.add(ambient_light);
*/
    
    controls = new THREE.TrackballControls( camera );
    
    //RENDER
	var render = function () {
		requestAnimationFrame(render);
        
        controls.update();
        
        var timer = Date.now() * 0.0005;
        
        //camera.position.x = Math.cos( timer ) * 80;
		//camera.position.z = Math.sin( timer ) * 80;
      
        camera.lookAt( scene.position );
		//camera.position.y += 0.01;

		renderer.render(scene, camera);
	};
    
/*
	//Rotate the mesh based on mouse position
	document.body.onmousemove = function (e) {
		a.rotation.z = e.pageX / 100;
		a.rotation.x = e.pageY / 100;
	}

	// Click to toggle wireframe mode
	document.body.onclick = function(e){
		a.material.wireframe = !a.material.wireframe;
	}
*/
	render();

}



function main(){

	init();

}
