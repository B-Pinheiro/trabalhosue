/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so2;

import java.rmi.Remote;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author bernardo
 */
public class QuestionariosServer {
    
        
    public static void main(String args[]) throws Exception {
  
        int regPort= 1099; 

        if (args.length != 5) { 
            System.out.println("Usage: java so2.rmi.PalavrasServer registryPort host bd user password");
            System.exit(1);
        }
	
        try {
           
            regPort=Integer.parseInt(args[0]);

            Questionarios q = new QuestionariosImpl(args[1],args[2], args[3], args[4]);
            
            
            java.rmi.registry.LocateRegistry.createRegistry(regPort);            
           
            
            java.rmi.registry.Registry registry = java.rmi.registry.LocateRegistry.getRegistry(regPort);

           
            registry.rebind("questionario", (Remote) q);  
            
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
             
	System.out.println("Bound RMI object in registry");

        System.out.println("servidor pronto");
     
    }
}
