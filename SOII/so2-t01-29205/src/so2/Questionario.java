
package so2;

import java.util.Vector;


public class Questionario implements java.io.Serializable{
    
    String nome;
    int respondido;
    
    public Questionario(String nome){
        this.nome = nome;
        respondido = 0;
    }
    
    public void setRespondido(int r){
        respondido = r;
    }
    
    public int getRespondido(){
        return respondido;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public String getNome(){
        return nome;
    }
    
    public String toString(){
        String str = "Nome:" + nome;
        return str;
    }
   
}