/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so2;

import java.util.Vector;

/**
 *
 * @author bernardo
 */
public interface Questionarios extends java.rmi.Remote {
    
    //Criar um questionario
    public void criarQuestionario(Questionario qt, Vector<String> pergunta) throws java.rmi.RemoteException;
    
    //Apagar um questionario
    public void apagaQuestionario(Questionario q) throws java.rmi.RemoteException;
    
    //Consultar os questionarios que existem
    public Vector<String> consultarQuestionario() throws java.rmi.RemoteException;
 
    //obter as perguntas de um questionario
    public Vector<String> obtemPerguntas(Questionario q) throws java.rmi.RemoteException; 
    
    //submeter respostar às questões de um questionario(todas as resposta numa)
    public void submeteQuestao(Questionario q, Vector<Integer> i) throws java.rmi.RemoteException;
    
    //consultar o numero de veze suq eum questionario foi respondido
    public int consultaQuestionarioRespondido(Questionario q) throws java.rmi.RemoteException;
    
    //obter a media do valor respondido em cada questão(para todas na mesma operação) de um quest. 
    public int obtemMedia(Questionario q) throws java.rmi.RemoteException;
      
    //Fechar a base de dados
    public void closeDB() throws java.rmi.RemoteException;
   
}
