/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so2;

import java.rmi.RemoteException;
import java.util.Scanner;
import java.util.Vector;

/**
 *
 * @author bernardo
 */
public class QuestionariosClient implements java.io.Serializable{
    
    public String regHost; //= "localhost";
    public String regPort; //= "9000";
    public Scanner sc;

    public QuestionariosClient(String regHost, String regPort){
        this.regHost = regHost;
        this.regPort = regPort;
        this.sc = new Scanner(System.in);
    }
    
    public void showmenu(){
        while (!sc.hasNextInt()){
                 
            System.out.println("input invalido!");
            System.out.println("Menu:\n"
                    + "1 -- Criar Questionario\n"
                    + "2 -- Consultar Questionario\n"
                    + "3 -- Apagar Questionario\n"
                    + "4 -- Obter Perguntas\n"
                    + "5 -- Submeter Respostas\n"
                    + "6 -- Consultar nº de vezes que um questionario foi respondido\n"
                    + "7 -- Obter a media das respostas\n");
            sc.next();
              
        }
    }
    
    public void menu(Questionarios q) throws RemoteException{
       
        
            System.out.println("Menu:\n"
                    + "1 -- Criar Questionario\n"
                    + "2 -- Consultar Questionario\n"
                    + "3 -- Apagar Questionario\n"
                    + "4 -- Obter Perguntas\n"
                    + "5 -- Submeter Respostas\n"
                    + "6 -- Consultar nº de vezes que um questionario foi respondido\n"
                    + "7 -- Obter a media das respostas\n");
          
            
        
        
        
        Questionario q1; 
        Vector<Integer> i = new Vector();
        int m = 0;
        String s;
        Vector<String> p = new Vector();
        
        while(true){
            
            showmenu();
            
            int a = sc.nextInt();
            switch(a){
                
                case 1: //1 -- Criar Questionario
                    
                    System.out.println("1 -- Criar Questionario");                 
                    System.out.println("Qual é o nome do Questionario? ");
                    s = sc.next();
                    
                    while(true){
                        System.out.println("Quantas perguntas quer inserir? ");
                        
                        while (!sc.hasNextInt()){
                            System.out.println("input invalido!");
                            System.out.println("Quantas perguntas quer inserir? ");
                            sc.next();
                        }  
                       
                        int z = sc.nextInt();
                        
                        if(z <= 5 && z >= 3){
                            for(int j = 0; j < z; j++){
                                System.out.print("Pergunta nº"+ (j+1) + ": ");
                                String str = sc.next(); 
                                p.add(str);
                            }
                            break;
                        }else{
                            System.out.println("O numero de perguntas não pode ser menor que 3 ou maior que 5");
                        }
                        
                    }
                    
                    q1 = new Questionario(s);
                    q.criarQuestionario(q1, p);
                    System.out.println("O questionario foi criado");
                    break;
                
                case 2: //2 -- Consultar Questionario
                    
                    System.out.println("2 -- Consultar Questionario");
                    Vector<String> stg = q.consultarQuestionario();
                    System.out.println("Questionarios disponiveis:");
                    for(String x : stg){
                        System.out.println("-" + x);
                    }
                    break;
                
                case 3: //3 -- Apagar Questionario
                    
                    System.out.println("3 -- Apagar Questionario");
                    System.out.println("Qual é o nome do Questionario que quer apagar?");
                    s = sc.next();
                    
                    q1 = new Questionario(s);
                    q.apagaQuestionario(q1);
                    System.out.println("O questionario foi apagado");
                                    
                    break;
            
                case 4: //4 -- Obter Perguntas
                    
                    System.out.println("4 -- Obter Perguntas");
                    System.out.println("Qual é o nome do Questionario?");
                    
                    s = sc.next();
                    q1 = new Questionario(s);
                    Vector<String> st = q.obtemPerguntas(q1);
                    
                    if(st.isEmpty()){
                        System.out.println("Não existem perguntas para esse questionario");
                    }
                    
                    for(int j = 0; j < st.size(); j++){
                        System.out.println(st.get(j));
                        System.out.print("Insira a sua Resposta: ");
                                               
                        while(true){
                            int b = sc.nextInt();
                            if(b <= 10 && b >= 1){
                                i.add(b);
                                break;
                            }else{
                                System.out.println("A resposta não pode ser menor que 1 ou maior que 10");
                                System.out.print("Insira a sua Resposta: ");
                            }
                        }
                        
                    }
                    
                    break;
            
                case 5: //5 -- Submeter Respostas
                    
                    System.out.println("5 -- Submeter Respostas");
                    System.out.println("Qual é o nome do Questionario?");
                    s = sc.next();
                    
                    q1 = new Questionario(s);
                    q.submeteQuestao(q1, i);
                    System.out.println("As respostas foram submetidas");
                    
                    break;
            
                case 6: //6 -- Consultar nº de vezes que um questionario foi respondido
                    
                    System.out.println("6 -- Consultar nº de vezes que um questionario foi respondido");
                    System.out.println("Qual é o nome do Questionario?");
                    s = sc.next();
                    
                    q1 = new Questionario(s);
                    m = q.consultaQuestionarioRespondido(q1);
                    System.out.println("O questionario " + q1.getNome() + " foi respondido " + m + " vezes.");
                    break;
            
                case 7: //7 -- Obter a media das respostas
                    
                    System.out.println("7 -- Obter a media das respostas");
                    System.out.println("Qual é o nome do Questionario?");
                    s = sc.next();
                    
                    q1 = new Questionario(s);
                    m = q.obtemMedia(q1);
                    System.out.println("A media para o questionario " + q1.getNome() + " é: " + m);
                    break;
            
                default:
                    break;
                
            }
    
        }

       
    }
    
    public void run(){
        try{
            Questionarios q =
                (Questionarios) java.rmi.Naming.lookup("rmi://" + regHost + ":" + 
                                		  regPort + "/questionario");

            menu(q);
            
        } catch (Exception ex) {
            ex.printStackTrace();
	}
        
    }
    
    public static void main(String args[]) {
        
	if (args.length !=2) { 
	    System.out.println
		("Usage: java so2.rmi.PalavrasClient registryHost registryPort");
	    System.exit(1);
	}
        
	QuestionariosClient qc = new QuestionariosClient(args[0], args[1]);
	qc.run();
        

    }  
        
}
