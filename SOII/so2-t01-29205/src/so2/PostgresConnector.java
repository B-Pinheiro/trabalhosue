package so2;

import java.sql.*;
import java.util.Vector;

/**
 *
 * @author bernardo
 */
public class PostgresConnector {

    private String PG_HOST;
    private String PG_DB;
    private String USER;
    private String PWD;

    Connection con = null;
    Statement stmt = null;

    public PostgresConnector(String host, String db, String user, String pw) {
        PG_HOST=host;
        PG_DB= db;
        USER=user;
        PWD= pw;
    }

    public void connect() throws Exception {
        try {
            Class.forName("org.postgresql.Driver");
            // url = "jdbc:postgresql://host:port/database",
            con = DriverManager.getConnection("jdbc:postgresql://" + PG_HOST + ":5432/" + PG_DB,
                    USER,
                    PWD);

            stmt = con.createStatement();

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Problems setting the connection");
        }
    }
    
    public void disconnect() {    // importante: fechar a ligacao 'a BD
        try {
            stmt.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Statement getStatement() {
        return stmt;
    }

   
    public void queryCriaQuestionario(Questionario q, Vector<String> pergunta){
        
        try{        
            
            stmt.executeUpdate("insert into questionarios (nome, numperguntas, numrespondido) values " 
                + "( '"+ q.getNome() + "', " + pergunta.size() + ", " + q.getRespondido() + ")");
            
            for( int i = 0; i < pergunta.size(); i++){
                stmt.executeUpdate("insert into perguntas (nomequest, idpergunta, pergunta ) values " 
                    + "( '" + q.getNome() + "', " + i + ", '" +  pergunta.elementAt(i) + "' )"  );
            
            }          
            
           
        } catch(Exception e){
            System.out.println("Problema a criar questionario");
        }
        
              
    }
    
    
    public void queryApagaQuestionario(Questionario q){

        try{
            
            stmt.executeUpdate("delete from questionarios where nome=" + "'" + q.getNome() + "'");
            
            stmt.executeUpdate("delete from perguntas where nomequest=" + "'" + q.getNome() + "'");
          
            stmt.executeUpdate("delete from respostas where nomequest=" + "'" + q.getNome() + "'");
 
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    
    public Vector<String> queryConsultaQuestionarios(){
        
        String query = "SELECT nome from questionarios";
        Vector<String> nomes = new Vector();
        
        try{        
          
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
           
                String nome = rs.getString("nome");
                nomes.add(nome);
            }
            
            rs.close();
            
        } catch(Exception e){
            e.printStackTrace();
        }
    
        return nomes;
        
    }
    
    
    public Vector<String> queryObtemPerguntas(Questionario q){
        
        String query = "SELECT pergunta from perguntas where nomequest =" + "'" + q.getNome() + "'";
        Vector<String> s = new Vector();
        
        try{        
          
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
               s.add(rs.getString("pergunta"));
            }
            
            rs.close();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        return s;
        
    }
    
    
    public void querySubmeteQuestao(Questionario q, Vector<Integer> i){
        
        int num = 0;
        
        for(int n = 0; n < i.size(); n++){
            
            String query = "insert into respostas (nomequest, idpergunta, resposta) values "
                    + "( '" + q.getNome() +"', "+ n + ", " + i.get(n) + ")";
            
            try{
                
                stmt.executeUpdate(query);
                            
            } catch(Exception e){
                e.printStackTrace();
            }
            
            
        }
         
        try{
            
            ResultSet rs = stmt.executeQuery("select numrespondido from questionarios where nome =" + "'" + q.getNome() + "'");
            
            while(rs.next()){
               num = rs.getInt("numrespondido");
            }
            num++;
            rs.close();
              
            stmt.executeUpdate("update questionarios set numrespondido ="
                    + num + " where nome =" + "'" + q.getNome() + "'");
                               
        } catch(Exception e){
            e.printStackTrace();
        }

    }
    
    
    public int queryConsultaQuestionarioRespondido(Questionario q){
        
        String query = "SELECT numrespondido from questionarios where nome =" + "'" + q.getNome() + "'";
        int respondido = 0;
        
        try{        
          
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                respondido = rs.getInt("numrespondido"); 
            }
            
            rs.close();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        return respondido;
    
    }
    
            
            
    public int queryObtemMedia(Questionario q){
        
        String query = "SELECT resposta from respostas where nomequest =" + "'" + q.getNome() + "'";
        int media = 0;
        int counter = 0;
        
        try{        
          
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                media += rs.getInt("resposta"); 
                counter++;
            }
            
            rs.close();
            
        } catch(Exception e){
            e.printStackTrace();
        }
        
        if(counter > 0 && media > 0){
            media = media / counter;
        }
        
        return media;
    
    }
    
    
}
