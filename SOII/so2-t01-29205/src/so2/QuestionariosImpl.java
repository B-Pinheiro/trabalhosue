
package so2;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;


public class QuestionariosImpl extends UnicastRemoteObject implements Questionarios, java.io.Serializable {
    
    PostgresConnector db;
    
         
    public QuestionariosImpl(String host, String datab, String user, String pw) throws RemoteException, Exception{
        super();
        db = new PostgresConnector(host, datab, user, pw);
        db.connect();
    }
   
   
    public void criarQuestionario(Questionario q, Vector<String> pergunta) throws RemoteException{
        db.queryCriaQuestionario(q, pergunta);
    }
    
    
    public void apagaQuestionario(Questionario q) throws RemoteException{
        db.queryApagaQuestionario(q);
    }
    
    
    public Vector<String> consultarQuestionario() throws RemoteException{
        return db.queryConsultaQuestionarios();     
    }
    
        
    public Vector<String> obtemPerguntas(Questionario q) throws RemoteException {
        return db.queryObtemPerguntas(q);
    }
    
    
    public void submeteQuestao(Questionario q, Vector<Integer> i) throws RemoteException {
        db.querySubmeteQuestao(q, i);
    }

           
    public int consultaQuestionarioRespondido(Questionario q) throws RemoteException{
        return db.queryConsultaQuestionarioRespondido(q);
    }
    
     
    public int obtemMedia(Questionario q) throws RemoteException{
        return db.queryObtemMedia(q);
    }
    
    
    public void closeDB(){
        db.disconnect();
    }

    
}
