<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionarios</title>
        <link rel="stylesheet" href="css/estilo.css">
    </head>
    <body>
        
        <h1>Questionario!</h1>
        
        <h2>Menu</h2>
        <p> 1 -- Criar Questionario </p>
        <p> 2 -- Consultar Questionario </p>
        <p> 3 -- Apagar Questionario </p>
        <p> 4 -- Obter Perguntas </p>
        <p> 5 -- Submeter Respostas </p>
        <p> 6 -- Consultar nº de vezes que um questionario foi respondido </p>
        <p> 7 -- Obter a media das respostas</p>
        
        <br>
        
        <form name="questionario" action="quest" method="POST">
            <input type="text" name="input" size="30" >
            <input type="submit" value="escolha" >    
        </form>
        
      
        <hr>
        
        <!-- script JSP para mostrar o IP -->
        <% 
            out.println("<p>Origem do pedido: "
                +request.getRemoteAddr()+"</p>");
        %>

    </body>
</html>
