
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Obter a media de um Questionario</h2>
            <%
                try {
                    Vector<String> n = dataManager.getQuestionarios();
                    String a = request.getParameter("nome");
                    
                     //Verifica se o questionario existe
                    if(n.contains(a)){
                    
                        Questionario q = new Questionario(a);
                        Vector<String> perg = dataManager.getPerguntas(q);
                         
                        for(int i = 0; i < perg.size(); i++){
                            int media = dataManager.getMedia(q, i);
                            out.println("<p> A pergunta '" + perg.elementAt(i) + "' do questionario " + q.getNome() +" tem uma media de " + media + "</p>");
                        }
                        
                
                    }else {
                        out.println("O questionario " + a + " n�o existe!");
                    }
                    
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
            
        </div> 
        
    </body>
</html>