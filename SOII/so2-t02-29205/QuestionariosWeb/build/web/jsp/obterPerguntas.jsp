
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Perguntas de um Questionario</h2>
            <%
                Vector<String> p = new Vector();
                Vector<String> n = new Vector(); 
                String a = "";

                try {
                    n = dataManager.getQuestionarios();
                    a = request.getParameter("nome");
                    
                    //Verifica se o questionario existe
                    if(n.contains(a)){
                    
                        Questionario q = new Questionario(a);
                                   
                        p = dataManager.getPerguntas(q);
                        
                        //Vector<Integer> respostas = new Vector();

                        for (int i = 0; i < p.size(); i++){
                            out.println("<h4>Pergunta n�"+ (i+1) +"</h4>");
                            out.println("<p> - " + p.elementAt(i) + "</p>");
                 
                            //respostas.add(Integer.parseInt(request.getParameter("numper") + (i+1)));
                        }
                          
                        //out.println("<p><a href=\"/QuestionariosWeb/jsp/inputRespostas.jsp\">Responder</a></p>");
                    }else {
                        out.println("O questionario " + a + " n�o existe!");
                    }
                    
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            <% if(n.contains(a)){ %>
            <form action="/QuestionariosWeb/jsp/addRespostas.jsp" method="POST" >
                <input type="hidden" name="action" value="orderConfirmation"/>
                <table class="submitdata">
                    <tr>
                        <th colspan="2">Respostas �s Perguntas do questionario</th>
                    </tr>
                    <tr>
                        <td>Pergunta n�1:</td>
                        <td><input type="text" name="numper1"/></td>
                    </tr>
                    <tr>
                        <td>Pergunta n�2:</td>
                        <td><input type="text" name="numper2"/></td>
                    </tr>
                    <tr>
                        <td>Pergunta n�3:</td>
                        <td><input type="text" name="numper3"/></td>
                    </tr>
                    <% if(p.size() >= 4){  %>
                   
                    <tr>
                        <td>Pergunta n�4:</td>
                        <td><input type="text" name="numper4"/></td>
                    </tr>
                    <% } %>
                    <% if(p.size() == 5){  %>
                    <tr>
                        <td>Pergunta n�5:</td>
                        <td><input type="text" name="numper5"/></td>
                    </tr>
                    <% } %>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="submit" name="submit" /></td>
                    </tr>
                </table>
            </form>
            <% }   %>
            
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
        </div>  
        
    </body>
</html>