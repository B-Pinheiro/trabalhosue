
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Consultar Questionario</h2>
            <%
                try {
         
                    Vector<String> n = dataManager.getQuestionarios();

                    out.println("<h3>Questionarios:</h3>");
                    
                    if(n.isEmpty()){
                        out.println("N�o existem questionarios");
                    }
                    
                    for(String x : n){
                        out.println("<p> - " + x + "</p>");
                    }                
                    
                     
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            
            <p align="center"><a href="<%=application.getAttribute("base")%>">voltar ao inicio</a></p>
        </div>  
        
    </body>
</html>