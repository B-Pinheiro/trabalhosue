
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Criar Questionario</h2>
            <form action="jsp/criarQuest.jsp" method="POST" >
                <input type="hidden" name="action" value="orderConfirmation"/>
                <table class="submitdata">
                    <tr>
                        <th colspan="2">Nome do questionario</th>
                    </tr>
                    <tr>
                        <td>Nome:</td>
                        <td><input type="text" name="nomequest"/></td>
                    </tr>
                    <tr>
                        <th colspan="2">Perguntas do questionario</th>
                    </tr>
                    <tr>
                        <td>Pergunta1:</td>
                        <td><input type="text" name="nump1"/></td>
                    </tr>
                    <tr>
                        <td>Pergunta2:</td>
                        <td><input type="text" name="nump2"/></td>
                    </tr>
                    <tr>
                        <td>Pergunta3:</td>
                        <td><input type="text" name="nump3"/></td>
                    </tr>
                    <tr>
                        <td>Pergunta4:</td>
                        <td><input type="text" name="nump4"/></td>
                    </tr>
                    <tr>
                        <td>Pergunta5:</td>
                        <td><input type="text" name="nump5"/></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="submit" value="submit" name="submit" /></td>
                    </tr>
                </table>
            </form>
              
                      
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
        </div>  
        
    </body>
</html>