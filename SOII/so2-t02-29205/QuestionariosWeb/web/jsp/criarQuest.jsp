
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Criar Questionario</h2>
            
            <%
                try {
                    
                    Vector<String> n = dataManager.getQuestionarios();
                    String a = request.getParameter("nomequest");
                    
                    //Verifica se o questionario existe
                    if(!n.contains(a)){
                        
                    
                        Questionario q = new Questionario(a);
                                
                        Vector<String> p = new Vector();
                                      
                        if(request.getParameter("nump1") != ""){
                            p.add(request.getParameter("nump1"));
                        
                        }
                        if(request.getParameter("nump2") != ""){
                            p.add(request.getParameter("nump2"));
                        }
                        if(request.getParameter("nump3") != ""){
                            p.add(request.getParameter("nump3"));
                        }
                        if(request.getParameter("nump4") != ""){
                            p.add(request.getParameter("nump4"));
                        }
                        if(request.getParameter("nump5") != ""){
                            p.add(request.getParameter("nump5"));
                        }
                    
                       
                        dataManager.criarQuestionario(q, p);
                    
                        out.println("O questionario " + q.getNome() + " foi criado");
                    
                    }else {
                        out.println("O questionario " + a + " j� existe!");
                    }
                    
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
        </div>  
        
    </body>
</html>