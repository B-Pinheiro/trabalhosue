
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Submeter Respostas</h2>
            <%
                try {
                    
                    Vector<Integer> i = new Vector();
                    
                    Vector<String> n = dataManager.getQuestionarios();
                    String a = "bernardo";//request.getParameter("nome");
                    //out.println(a);
                    //Verifica se o questionario existe
                    if(n.contains(a)){
                        
                        Questionario q = new Questionario(a);
                        
                        //out.println(a);
                        Vector <Integer> respostas = new Vector();
                        respostas = (Vector<Integer>) request.getSession().getAttribute("respostas");
                        
                        if(!respostas.isEmpty()){
                        

                            dataManager.submeterPerguntas(q, respostas);
                    
                            out.println("As respostas ao questionario foram submetidas");
                      
                            respostas = new Vector(); 
                            request.getSession().setAttribute("respostas", respostas);   
                        
                        }else{
                            out.println("N�o existem respostas para submeter!!");
                        }
                        
                    } else {
                        out.println("O questionario " + a + " n�o existe!");
                    }
                    
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
            
        </div> 
        
    </body>
</html>