
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Consultar o numero de vezes que um questionario foi respondido</h2>
            <%
                try {
                    Vector<String> n = dataManager.getQuestionarios();
                    String a = request.getParameter("nome");
                    
                    //Verifica se o questionario existe
                    if(n.contains(a)){
                        
                        Questionario q = new Questionario(a);
                    
                        int respondido = dataManager.getRespondido(q);
                    
                        out.println("O questionario " + q.getNome() +" foi respondido " + respondido + " vezes");
                    
                    }else {
                        out.println("O questionario " + a + " n�o existe!");
                    }
                    
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
        </div> 
        
    </body>
</html>