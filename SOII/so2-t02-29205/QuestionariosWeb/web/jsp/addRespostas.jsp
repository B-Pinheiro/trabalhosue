
<%@page import="java.util.Vector"%>
<%@page language="java" contentType="text/html"%>
<%@page import="quest.beans.Questionario"%>

<jsp:useBean id="dataManager" scope="application" class="quest.model.DataManager"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Questionario</title>
         <link rel="stylesheet" href="css/estilo.css" type="text/css"/>
    </head>
    
    <body>
        <div class="content">
            <h2>Adicionar as Respostas</h2>
            <%
                try {                       
                    Vector<Integer> respostas = new Vector();
                    
                    
                    for (int i = 0; i < 3; i++){ 
                        if(request.getParameter("numper"+ (i+1)) != ""){
                            respostas.add(Integer.parseInt(request.getParameter("numper" + (i+1))));
                            out.print("<p> Resposta n�" + (i+1) + ": " + request.getParameter("numper" + (i+1)) + "</p>");
                        }
                    }
                    
                    request.getSession().setAttribute("respostas", respostas);      
                    out.println("<p><a href=\"/QuestionariosWeb/quest\">Responder</a></p>");
                    
                    
                    
                } catch (Exception e) {
            %><p class="error">Questionario Invalido!</p><%
            }
            %>
            
            
            <p align="center"><a href="/QuestionariosWeb/quest">voltar ao inicio</a></p>
        </div>  
        
    </body>
</html>