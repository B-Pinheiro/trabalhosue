/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quest.model;

import quest.beans.Questionario;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;



public class PostgresConnector implements QuestionarioLogic{
    
    Connection connection = null;
    private String PG_HOST;
    private String PG_DB;
    private String USER;
    private String PWD;
    
    public PostgresConnector(String host, String db, String user, String pw){
        PG_HOST= host;
        PG_DB= db;
        USER= user;
        PWD= pw;
        
    }

    public void connect() throws Exception{
        
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace(System.out);
        }
        try {
            connection= DriverManager.getConnection("jdbc:postgresql://" + PG_HOST + ":5432/" + PG_DB,
                    USER,
                    PWD);
            
        }
        catch (SQLException e) {
            e.printStackTrace(System.out);
            System.err.println("Problems setting the connection");
        }
    }


    public void criarQuestionario(Questionario q, Vector<String> pergunta) {
        
        if(connection != null){
            
            try{
                
                Statement stmt = connection.createStatement();
             
                stmt.executeUpdate("insert into questionarios (nome, numperguntas, numrespondido) values " 
                + "( '"+ q.getNome() + "', " + pergunta.size() + ", " + q.getRespondido() + ")");
            
                for( int i = 0; i < pergunta.size(); i++){
                    stmt.executeUpdate("insert into perguntas (nomequest, idpergunta, pergunta) values " 
                    + "( '" + q.getNome() + "', " + i + ", '" +  pergunta.elementAt(i) + "')"  );
                }          
            
                stmt.close();
                
            } catch (SQLException e) {
                System.out.println("Could not get questionario: " + e.getMessage());
            }      
        }   
        
    }

    
    public Vector<String> getQuestionarios() {
       
        Vector<String> nomes = new Vector();
        
        if(connection != null){
            
            try{
            
                Statement stmt = connection.createStatement();
                
                try{        
                    
                    String query = "SELECT nome from questionarios";
                    ResultSet rs = stmt.executeQuery(query);
            
                    while(rs.next()){
           
                        String nome = rs.getString("nome");
                        nomes.add(nome);
                    }
            
                    rs.close();
            
                } catch(Exception e){
                    e.printStackTrace();
                }
                
            } catch (SQLException e) {
                System.out.println("Could not get questionario: " + e.getMessage());
            }
        
        }
        
        return nomes;
        
    }
    
    
    public void apagaQuestionario(Questionario q) {
        
        if(connection != null){
            
            try{
            
                Statement stmt = connection.createStatement();
                
                stmt.executeUpdate("delete from questionarios where nome=" + "'" + q.getNome() + "'");
            
                stmt.executeUpdate("delete from perguntas where nomequest=" + "'" + q.getNome() + "'");
          
                stmt.executeUpdate("delete from respostas where nomequest=" + "'" + q.getNome() + "'");
 
            
            } catch (SQLException e) {
                System.out.println("Could not get questionario: " + e.getMessage());
            }
        }
        
    }

    
    public Vector<String> getPerguntas(Questionario q) {
        
        String query = "SELECT pergunta from perguntas where nomequest =" + "'" + q.getNome() + "'";
        Vector<String> s = new Vector();
        
        if(connection != null){
            
            try{        
                
                Statement stmt = connection.createStatement();
                
                ResultSet rs = stmt.executeQuery(query);
            
                while(rs.next()){
                    s.add(rs.getString("pergunta"));
                }
            
                rs.close();
            
            } catch (SQLException e) {
                System.out.println("Could not get questionario: " + e.getMessage());
            }
        }
        
        return s;
        
    }


    public void submeterPerguntas(Questionario q, Vector<Integer> i) {
        
        if(connection != null){
                     
            int num = 0;
        
            for(int n = 0; n < i.size(); n++){
            
                String query = "insert into respostas (nomequest, idpergunta, resposta) values "
                    + "( '" + q.getNome() +"', "+ n + ", " + i.get(n) + ")";
            
                try{
                    
                    Statement stmt = connection.createStatement();
                    
                    stmt.executeUpdate(query);
                            
                } catch (SQLException e) {
                    System.out.println("Could not get questionario: " + e.getMessage());
                }
            
            
            }
         
            try{
                Statement stmt = connection.createStatement();
                
                ResultSet rs = stmt.executeQuery("select numrespondido from questionarios where nome =" + "'" + q.getNome() + "'");
            
                while(rs.next()){
                    num = rs.getInt("numrespondido");
                }
                num++;
                rs.close();
              
                stmt.executeUpdate("update questionarios set numrespondido ="
                    + num + " where nome =" + "'" + q.getNome() + "'");
                               
            } catch (SQLException e) {
                System.out.println("Could not get questionario: " + e.getMessage());
            }
        
        }
        
    }

    
    public int getRespondido(Questionario q) {
        
        int respondido = 0;
        
        if(connection != null){
            
            String query = "SELECT numrespondido from questionarios where nome =" + "'" + q.getNome() + "'";
          
            try{        
                
                Statement stmt = connection.createStatement();
                
                ResultSet rs = stmt.executeQuery(query);
            
                while(rs.next()){
                    respondido = rs.getInt("numrespondido"); 
                }
            
                rs.close();
            
            } catch (SQLException e) {
                System.out.println("Could not get questionario: " + e.getMessage());
            }
        
        }
        
        return respondido;
        
    }

  
    public int getMedia(Questionario q, int i) {
       
        int media = 0;
        int counter = 0;
        
        if(connection != null){
            
            try{    
                
                Statement stmt = connection.createStatement();
                
                String query = "SELECT resposta from respostas where nomequest =" + "'" + q.getNome() + "' and where idpergunta=" + i;
                
               /* String query2 = "SELECT idpergunta from respostas where nomequest =" + "'" + q.getNome() + "'";
                
                String query3 = "SELECT idpergunta from perguntas where nomequest =" + "'" + q.getNome() + "'";
                
                ResultSet r2 = stmt.executeQuery(query2);
                 
                ResultSet r3 = stmt.executeQuery(query3);

                */
                ResultSet rs = stmt.executeQuery(query);
                
                while(rs.next()){
                    int a = Integer.parseInt(rs.getString("resposta")); 
                    media += a;
                    counter++;
                }
            
                rs.close();
            
            } catch(Exception e){
                e.printStackTrace();
            }
        
            if(counter > 0 && media > 0){
                media = media / counter;
            }
        
        }

        return media;
       
    }


    public void finish() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    
}
