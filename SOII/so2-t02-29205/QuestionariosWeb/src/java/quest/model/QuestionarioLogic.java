
package quest.model;

import quest.beans.Questionario;

import java.util.Vector;

/**
 *
 * @author bernardo
 */
public interface QuestionarioLogic {
    
    //--- Criar um questionario ---
    public abstract void criarQuestionario(Questionario q, Vector<String> pergunta);
    
    //--- Consultar questionarios ---
    public abstract Vector<String> getQuestionarios();
    
    //--- Apagar um questionario ---
    public abstract void apagaQuestionario(Questionario q);
    
    //--- Obter perguntas ---
    public abstract Vector<String> getPerguntas(Questionario q);
    
    //--- Submeter respostas às questoes
    public abstract void submeterPerguntas(Questionario q, Vector<Integer> i);
    
    //--- Consultar o numero de vezes que um questionario foi respondido ---
    public abstract int getRespondido(Questionario q);
    
    //--- Obter a media de um Questionario ---
    public abstract int getMedia(Questionario q, int i);
    
    public void finish();
    
}
