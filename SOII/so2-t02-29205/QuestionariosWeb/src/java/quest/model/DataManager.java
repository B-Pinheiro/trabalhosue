package quest.model;

import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Vector;
import javax.servlet.ServletContext;
import quest.beans.Questionario;


public class DataManager implements QuestionarioLogic{

    private static PostgresConnector dbManager;

    public DataManager() {
        
        try{
            dbManager = new PostgresConnector("alunos.di.uevora.pt", "l29205", "l29205", "novaPass"); 
            dbManager.connect();
            
        }catch(Exception e){
            e.printStackTrace();
        }
             
    }
    

    public void criarQuestionario(Questionario q, Vector<String> pergunta) {
        dbManager.criarQuestionario(q, pergunta);
    }

    public Vector<String> getQuestionarios() {
        return dbManager.getQuestionarios();
    }

    public void apagaQuestionario(Questionario q) {
        dbManager.apagaQuestionario(q);
    }

    public Vector<String> getPerguntas(Questionario q) {
        return dbManager.getPerguntas(q);
    }

    public void submeterPerguntas(Questionario q, Vector<Integer> i) {
        dbManager.submeterPerguntas(q, i);
    }

    public int getRespondido(Questionario q) {
        return dbManager.getRespondido(q);
    }

    public int getMedia(Questionario q, int i) {
        return dbManager.getMedia(q, i);
    }

    public void finish() {
        dbManager.finish();
    }
    
}
