package quest.beans;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.QueryParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


@Path(value = "/quest")
public class QuestionarioResource {

    private Questionario quest;

    /**
     * This class is annotated with @Singleton meaning that only
     * one instance of this class will be instantated per web
     * application. 
     */
    public QuestionarioResource() {
        quest = new Questionario();   
        quest.add(new Questao("Quem?"));
        quest.add(new Questao("Onde?"));
    }

    
    @GET
    @Produces({"application/json", "application/xml"})
    public synchronized Questionario getQuestionario() {
        return quest;
    }

    /* Sem a anotacao @Path, a escolha entre este metodo e o anterior faz-se pelo HTTP Method (GET/PUT)
    */
    @PUT
    @Consumes({"application/json", "application/xml"})
    public synchronized void putQuestionario(Questionario quest) {
        this.quest = quest;
    }

    /*
	Este método é invocado se o AppServer receber um Http POST com o sufixo /add, apos o URI base deste resource.
    */
    @Path("/add")
    @POST
    @Consumes({"application/json", "application/xml"})
    @Produces({"application/json", "application/xml"})
    public synchronized Questionario addQuestao(@QueryParam("pergunta") String pergunta) {
        quest.add(new Questao(pergunta));
	return quest;
    }


}

