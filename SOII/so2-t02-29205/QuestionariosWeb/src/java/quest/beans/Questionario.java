
package quest.beans;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name = "questionario")
public class Questionario {

    @XmlElement(required = true)
    protected List<Questao> questoes;
    
    String nome;
    
    int respondido;

    public Questionario() {
    	questoes = new LinkedList<Questao>();
    }	

   
    public List<Questao> getQuestao() {
        return this.questoes;
    }
    

    public void add( Questao a ) {
	questoes.add( a );
    }

    public int size() {
	return questoes.size();
    }

    
    public Questionario(String n){
        nome = n;
        respondido = 0;
    }

    public void setRespondido(int r){
        respondido = r;
    }
    
    public int getRespondido(){
        return respondido;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    
    public String getNome(){
        return nome;
    }
    
    public String toString(){
        String str = "Nome:" + nome;
        return str;
    }
   
}