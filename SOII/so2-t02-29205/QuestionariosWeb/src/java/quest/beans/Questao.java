package quest.beans;

import java.util.Formatter;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "questao", propOrder = {
    "pergunta"
})
@XmlRootElement(name = "questao")
public class Questao {

    @XmlElement(required = true)
    protected String pergunta;
   
    public Questao() {
    }

    public Questao(String pergunta) {
	this.pergunta = pergunta;
    }
  
    public String getPergunta() {
        return pergunta;
    }

    public void setNome(String pergunta) {
        this.pergunta = pergunta;
    }

}
