package quest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import quest.model.DataManager;
import quest.beans.Questionario;


/**
 *
 * @author bernardo
 */
public class Questionarios extends HttpServlet {
    
    DataManager dataManager; 
    
    public Questionarios(){
        super();
    }
    
    public void init(ServletConfig config) throws ServletException{
        
        super.init(config);
        System.out.println("*** initializing controller servlet.");  
        System.out.println("Aplicacao - CONTEXT Path: "+ getServletContext().getContextPath());

        ServletContext context = config.getServletContext();
        
        context.setAttribute("base", config.getInitParameter("base"));
        
        dataManager = new DataManager();
        
        // atributo com o objeto que gere o acesso aos dados (partilhado pelos componentes web)
        context.setAttribute("dataManager", dataManager);
             
        
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Questionarios</title>");          
            out.println("<link rel=\"stylesheet\" href=\"css/estilo.css\">");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Questionarios at " + request.getContextPath() + "</h1>");
            
            
            String base = "/jsp/";
            String url = base + "index.jsp";
            String escolha = request.getParameter("input");
            
            //Vector <Integer> respostas = new Vector();
            Questionario q;
            String s;
            
            if (escolha != null) {
                
                //Criar Questionario
                if (escolha.equals("1")) {       
                    url = base + "inputcriarQuest.jsp";
                } 
                
                //2 -- Consultar Questionario
                else if (escolha.equals("2")) {
                    url = base + "consultarQuest.jsp";    
                }
                
                //3 -- Apagar Questionario
                else if (escolha.equals("3")) {
                     url = base + "inputNome.jsp";
                } 
                
                //4 -- Obter Perguntas
                else if (escolha.equals("4")) {
                    url = base + "inputNome.jsp";
                } 
                
                //5 -- Submeter Respostas
                else if (escolha.equals("5")) {
                    url = base + "inputNome.jsp";
                } 
                
                //6 -- Consultar nº de vezes que um questionario foi respondido
                else if (escolha.equals("6")) {
                    url = base + "inputNome.jsp";
                }
                
                //7 -- Obter a media das respostas
                else if (escolha.equals("7")) {
                    url = base + "inputNome.jsp";
                }
                
                
                else{
                    
                    out.println("Escolha Invalida!!");
                    
                    out.println("<link rel=\"stylesheet\" href=\"css/estilo.css\">");
                    
                    out.println(" <h2>Menu</h2>\n" +
                    "        <p> 1 -- Criar Questionario </p>\n" +
                    "        <p> 2 -- Consultar Questionario </p>\n" +
                    "        <p> 3 -- Apagar Questionario </p>\n" +
                    "        <p> 4 -- Obter Perguntas </p>\n" +
                    "        <p> 5 -- Submeter Respostas </p>\n" +
                    "        <p> 6 -- Consultar nº de vezes que um questionario foi respondido </p>\n" +
                    "        <p> 7 -- Obter a media das respostas</p>");
                    
                    out.println("  <form name=\"questionario\" action=\"jsp/index.jsp\" method=\"POST\">\n" +
                    "            <input type=\"text\" name=\"input\" size=\"30\">\n" +
                    "            <input type=\"submit\" value=\"escolha\">    \n" +
                    "        </form>");
                    
                    
                    escolha = request.getParameter("input");
                }
                
            }
        

            RequestDispatcher requestDispatcher
                = getServletContext().getRequestDispatcher(url);
            requestDispatcher.forward(request, response);
            
               
            out.println("<p> </p>");
            out.println("<a href=\"javascript: history.go(-1)\">voltar</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    
    @Override
    public void destroy() {
        ServletContext context=this.getServletContext();
        DataManager dataManager= (DataManager) context.getAttribute("dataManager");
        if (dataManager !=null) {
            try {
                /**
                 * terminar a ligacao 'a BD de modo seguro:
                 */
                dataManager.finish();
            }
            catch (Exception e) {
                System.err.println("PROBLEMA AO FINALIZAR: "+e.getMessage());
            }
        }
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
