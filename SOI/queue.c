#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"


queue* queue_new()
{
   queue *q = malloc(sizeof(queue));
   q->head = NULL;
   q->size = 0;

   return q;
}


node *new_node(PCB *p)
{
   node *n = malloc(sizeof(node));

   n->pcb = p;
   n->next = NULL;

   return n;
}


void enqueue(queue *q, PCB *p)
{

   if(q->head != NULL){
      node *n = q->head;

      while(n->next != NULL){
         n = n->next;
      }

      n->next = new_node(p);

   }else {
      q->head = new_node(p);
   }

   q->size++;

}


PCB* dequeue(queue *q)
{
   node *n = q->head;

   if(n != NULL && n->next != NULL){

      q->head = q->head->next;

      PCB *p = n->pcb;

      free(n);
      q->size--;

      return p;

   }else if (n != NULL){

      PCB *p = q->head->pcb;
      q->head = NULL;
      q->size--;

      return p;

   }else{
      printf("erro\n");
      return;
   }


}


int queue_size(queue *q)
{
   return q->size;
}


PCB* queue_first(queue *q)
{
   return (q->head->pcb);
}


void queue_print(queue *q)
{
   if(queue_size(q) > 0){
      node *n = q->head;

      while(n->next != NULL && n->pcb != NULL){
         printf("membegin: %d memend: %d pc: %d\n", n->pcb->membegin,n->pcb->memend, n->pcb->pc);
         n = n->next;
      }
      printf("membegin: %d memend: %d pc: %d\n", n->pcb->membegin,n->pcb->memend, n->pcb->pc);

   }else{
      printf("A queue esta vazia!!\n");
   }

}
