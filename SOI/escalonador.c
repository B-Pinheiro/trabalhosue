#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "escalonador.h"


//Create a new pcb
PCB *new_pcb(int membegin, int memend, int pc)
{
   PCB *p = malloc(sizeof(PCB));
   p->membegin = membegin;
   p->memend = memend;
   p->pc = pc;

   return p;
}


//Destroy a pcb
void pcb_destroy(PCB *pcb)
{
   free(pcb);
}

// Create a new Process
process *new_process(int inst[1024], int arrival)
{
   process *p = malloc(sizeof(process));

   int i = 0;
   while(i < 1024){
      if(inst[i] == 10){
         p->inst[i] = 10;
         break;
      }
      p->inst[i] = inst[i];
      i++;
   }

   p->arrival = arrival;

   return p;
}

//Destroy a process
void process_destroy(process *p)
{
   free(p);
}

//Write instructions to memory
PCB *process_write_mem(process *p, void *mem[])
{

   int i = 0;
   int counter = 0;
   int n;
   //Get the index of memory to write
   while(i < MEMSIZE){
      if(mem[i] == NULL){
         break;
      }
      i++;
   }

   while(counter < 1024){
      if(p->inst[counter] == 10){
         break;
      }
      counter++;
   }

   if( ((i+10+counter) <= MEMSIZE)){// && ((10+counter) == c) ){

      int membegin = i; //Memory Start for this pcb
      int pc = i+10;   //Pc start (+10 to skip variables)


      for(n = 0; n < 10; n++){
         int a = 48;
         mem[i] = a;
         i++;
      }

      n = 0;
      while(n < 1024){
         if(p->inst[n] == 10){
            break;
         }

         char *s = (char *) malloc(3);
         s[0] = p->inst[n] + 48;
         n++;
         s[1] = p->inst[n] + 48;
         s[2] = 0;

         mem[i] = (char *) s;

         n++;
         i++;
      }

      PCB *pcb = new_pcb(membegin, i-1, pc);

      process_destroy(p);

      return pcb;

   }else{

      printf("Memoria Cheia\n");
      return NULL;

   }

}
