#ifndef _ESCALONADOR_
#define _ESCALONADOR_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

#define QUANTUM 4
#define MAXNEW 4
#define MEMSIZE 300


typedef struct PCB
{
   int membegin;
   int memend;
   int pc;
} PCB;


typedef struct process
{
   int inst[1024]; //Intruções
   int arrival;    //Instante em que o processo chega
} process;

PCB *new_pcb(int membegin, int memen ,int pc);
void pcb_destroy(PCB *pcb);

process *new_process(int inst[], int arrive);
void process_destroy(process *p);
PCB *process_write_mem(process *p, void *mem[]);

#endif
