#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_w.h"

void read_file(FILE *file, process *pc[])
{
   char* linha = NULL;
   size_t len = 0;
   ssize_t read;
   char *p;
   int inst[1024];
   int arrival;
   int counter_pc = 0;
   int i;
   char delimit[]=" \t\r\n\v\f";

   if(file == NULL){
      printf("\nFicheiro Nao Encontrado");

   } else{

      while((read = getline(&linha, &len, file)) != -1){
         linha[strlen(linha)-1] = 0;

         int ii = 0;

         p = strtok(linha, delimit);

         arrival = atoi( p );
         p = strtok(NULL, delimit);

         while(p != NULL){

            int a = strlen(p);

            for(i = 0; i < a; i++){
               inst[ii] = *p - 48;
               p++;
               ii++;
            }

            p = strtok(NULL, delimit);

         }
         inst[ii] = 10;
         
         pc[counter_pc] = new_process(inst, arrival);
         counter_pc++;
      }

   }
   fclose(file);

}

void write_file(FILE *file, char *text)
{
   fprintf(file, "%s", text);
   printf("Foi escrito: %s", text);

   fclose(file);
}
