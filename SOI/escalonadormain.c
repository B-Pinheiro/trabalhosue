#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file_w.h"
#include "queue.h"


int block = 0;
void* mem[MEMSIZE];


// Global = 0 -> Normal
// Global = 1 -> Debug
#define Global 1


void debug(queue *q_new, queue *q_ready, queue *q_run, queue *q_blocked, queue *q_exit){
   int ci;
   for(ci = 0; ci < 300; ci++){
      //if(mem[ci] != NULL){
      printf("Posição: %d Memoria: %p\n", ci, mem[ci]);
      //}
      
   }
   printf("Queue - New\n");
   queue_print(q_new);
   printf("\n");

   printf("Queue - Ready\n");
   queue_print(q_ready);
   printf("\n");

   printf("Queue - Run\n");
   queue_print(q_run);
   printf("\n");

   printf("Queue - Blocked\n");
   queue_print(q_blocked);
   printf("\n");

   printf("Queue - Exit\n");
   queue_print(q_exit);
   printf("\n");

}


void check_new(process *pc[], int a[32])
{
   int i;
   for(i = 0; i < 32; i++){
      if(pc[i] != NULL){
         a[i] = pc[i]->arrival;
      }else{
         a[i] = -1;
      }
   }

}


int check_blocked(queue *q){

   if(queue_size(q) == 0){
      return 0;
   }
   return 1;
}

int check_ready(queue *q)
{
   if(queue_size(q) < 4){
      return 1;
   }

   return 0;
}


int check_run(queue *q)
{
   if(queue_size(q) == 0){
      return 1;
   }

   return 0;
}

int check_exit(queue *q)
{
   if(queue_size(q) > 0){
      return 1;
   }

   return 0;
}



void run(PCB *p, void *mem[], queue *q_exit)
{

   int pc = p->pc;
   int membegin = p->membegin;

   char *s = mem[pc];

   int a;

   char x = (char) s[0];
   char y = (char) s[1];
   int index = y - 48;

   switch (x){

      case 48:
         mem[membegin+index] = (int *) 48;
         p->pc++;
         break;

      case 49:
         a = mem[membegin+index];
         a++;
         mem[membegin+index] = a;
         p->pc++;
         break;

      case 50:
         a = mem[membegin+index];
         a--;
         mem[membegin+index] = a;
         p->pc++;
         break;

      case 51:
         if(mem[membegin+index] == 0){
            pc++;
         }
         else{
            pc+=2;
         }
         p->pc = pc;
         break;

      case 52:
         pc -= index;
         p->pc = pc;
         break;

      case 53:
         pc += index;
         p->pc = pc;
         break;

      case 55:
         block = 1;
         p->pc++;
         break;

      case 56:
         mem[membegin] = mem[membegin+index];
         p->pc++;
         break;

      case 57:
         enqueue(q_exit, p);
         p->pc++;
         break;

      default:
         break;

   }

}




int main(){

   FILE *file;
   int esc_time = 0;
   int temp_quantum = 0;
   int i;


   //Create the queues
   queue *q_new = queue_new();
   queue *q_ready = queue_new();
   queue *q_blocked = queue_new();
   queue *q_run = queue_new();
   queue *q_exit = queue_new();

   //Array of process
   process *pc[32];

   //Initialize the array
   for(i = 0; i < 32; i++){
      pc[i] = NULL;
   }

   //Open the file
   file = fopen("input.xpto", "r");

   //Initialize the memory with NULL
   for(i = 0; i < MEMSIZE; i++){
      mem[i] = NULL;
   }

   //Read from file and get array of processes
   read_file(file, pc);


   //Array with process arrivals
   int b[32];
   check_new(pc, b);


   //int block_counter = 0;
   for(esc_time = 0; esc_time < 10000; esc_time++){

      #ifdef Global
      #if Global
         if(esc_time < 10){
            printf("\n");
            printf("Instante: %d\n", esc_time);
            printf("\n");
            debug(q_new, q_ready, q_run, q_blocked, q_exit);
         }
      #endif
      #endif
      //BLOCKED
      //Verifica se o blocked contem algum processo
/*
      if(check_blocked(q_blocked)){

         block_counter++;
         if(block_counter == 4){

            PCB *p_blocked = dequeue(q_blocked);
            enqueue(q_ready, p_blocked);

            block_counter = 0;
         }

      }
*/

      //NEW
      //Procura um processo com o arrival == time e devolve o index
      for(i = 0; i < 32; i++){
         if(b[i] == esc_time){
            printf("Processo entrou no new no instante: %d\n", b[i]);
            PCB *pcb = process_write_mem(pc[i], mem);
            enqueue(q_new, pcb);
            break;
         }
      }


      //READY
      //Verifica se o ready já tem mais de 4 processos
      if(check_ready(q_ready) && queue_size(q_new) > 0){

         PCB *pb = dequeue(q_new);
         enqueue(q_ready, pb);
      }

      //RUN
      //Verifica se o run ja tem algum processo a correr
      if(check_run(q_run) && queue_size(q_ready) > 0){

         PCB *p = dequeue(q_ready);

         run(p, mem, q_exit);

         //Verificar se o processo foi para o blocked
/*
         if(block == 1){
            printf("blocked\n");
            //PCB *pp = dequeue(q_run);
            enqueue(q_blocked, p);
            //free(pp);
            block = 0;


      } else */
      //Verificar se o processo foi para o exit
         if(check_exit(q_exit)){

            PCB *p3 = dequeue(q_exit);
            printf("Um processo acabou!!\n");
            int ii;
            for(ii = p3->membegin; ii <= p3->memend; ii++){
               mem[ii] = NULL;
            }

            free(p3);

         //Continua normalmente
         } else{
            temp_quantum++;
            enqueue(q_run, p);
         }


      }else if(!check_run(q_run)){

         if(temp_quantum < QUANTUM){

            PCB *p = dequeue(q_run);
            run(p, mem, q_exit);

            /*if(block == 1){
               printf("blocked\n");
               //PCB *pp = dequeue(q_run);
               enqueue(q_blocked, p);
               //free(pp);
               block = 0;


         } else */
            //Verificar se o processo foi para o exit
            if(check_exit(q_exit)){

               PCB *p3 = dequeue(q_exit);
               printf("Um processo acabou!!\n");

               int ii;
               for(ii = p3->membegin; ii <= p3->memend; ii++){
                  mem[ii] = NULL;
               }

               free(p3);

            //Continua normalmente
            } else{
               temp_quantum++;
               enqueue(q_run, p);
            }


         }else{

            PCB *p = dequeue(q_run);
            enqueue(q_ready, p);

            temp_quantum = 0;

            PCB *p2 = dequeue(q_ready);

            run(p2, mem, q_exit);

/*
            if(block == 1){
               printf("blocked\n");
               //PCB *pp = dequeue(q_run);
               enqueue(q_blocked, p);
               //free(pp);
               block = 0;


         } else*/
            //Verificar se o processo foi para o exit
            if(check_exit(q_exit)){

               PCB *p3 = dequeue(q_exit);
               printf("Um processo acabou!!\n");
               int ii;
               for(ii = p3->membegin; ii <= p3->memend; ii++){
                  mem[ii] = NULL;
               }

               free(p3);

            //Continua normalmente
            } else{
               temp_quantum++;
               enqueue(q_run, p2);

            }

         }

      }

   }


   return 0;
}
