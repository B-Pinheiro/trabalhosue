#ifndef _QUEUE_
#define _QUEUE_

#include "escalonador.h"


typedef struct node{
   struct PCB *pcb;
   struct node *next;
} node;


typedef struct queue{

	node *head;
	int size;

} queue;


queue* queue_new();
node *new_node();

void enqueue(queue *q, struct PCB *p);
struct PCB* dequeue(queue *q);
int queue_size(queue *q);
struct PCB* queue_first(queue *q);
void queue_print(queue *q);

#endif
