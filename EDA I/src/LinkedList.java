
public class LinkedList<T> {

	Node<T> header;
	int counter = 0;
	
	public LinkedList(){
		header = new Node<T>();
	}
	
	public LinkedList(T x){
		header = new Node<T>(x);
	}
	
		
	public int size(){
		return counter; 
	}
	
	public boolean isEmpty(){
		return (counter == 0);
	}
	
	
	public void add(T x){
		
		Node<T> newnode = new Node<T>(x);
		
		newnode.next = header.next;
		header.next = newnode;
		
		counter ++;
	
	}
	
	public void remove(T x){
		
		Node<T> c = header;
		Node<T> d = header.next;
		
		while(d != null){
			
			if(d.elemento.equals(x) == true){
				c.next = d.next;
				counter --;
					
			}
			//	c.next.elemento = null;
			
				
			
			c = c.next;
			d = d.next;
			
		}		
		
	}
	
	public void removeLast(){
		
		Node<T> c = header;
		Node<T> d = header.next;
		
		//while(d != null){
			if(d.next != null){
				c.next = d.next;
				counter --;
			}
			//c = c.next;
			//d = d.next;
		//}
		
	}
	
	public boolean contains(T x){
		
		Node<T> c = header;
		
		while(c != null){		
			if(c.elemento != null)
				if(c.elemento.equals(x) == true)
					return true;
			
			c = c.next;
		}
		return false;
		
	}
	
	
	public void print(){
		Node<T> b = new Node<T>();
		
		b = header;
		
		for(int i = 0; i < counter; i ++ ){
			b = b.next;
			if(b.elemento != null)
				System.out.println(b.elemento);
		}
		
	}
	
	
	public static void main(String[] args){
		
		LinkedList<String> asd = new LinkedList<String>();
		
		
		asd.add("1");
		asd.add("2");
		asd.add("3");
		asd.add("4");
		asd.add("5");
		
		asd.print();
		
		System.out.println("asd");
		
		asd.remove("1");
		
		asd.print();
		
		
		
		
		
	}
	
	
	
}
