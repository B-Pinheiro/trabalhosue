
public class Node<T> {


	T elemento;
	Node<T> next;
	
	
	Node(){
		next = null;
	}
	
	Node(T e){
		elemento = e;
		next = null;
	}
	
	Node(T e, Node<T> next){
		elemento = e;
		this.next = next;
	}
	
	public void setElement(T e){
		elemento = e;
	}
	
	
	public void setNext(Node<T> n){
		next = n;
	
	}
	
	public Node<T> getNext(){
		return next; 
	}
	
	
	
}
