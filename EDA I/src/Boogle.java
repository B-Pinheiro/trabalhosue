import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Boogle {
	
	Position p;
	Board b;
	HashTable<String> hashWords;
	HashTable<String> words;
	
	LinkedList<String> positions;

	LinkedList<String> finalWords;
	
	ArrayStack<String> stackPos;
	
	ArrayStack<String> charToRemove;

	public Boogle(){
		
		b = new Board();
		
		hashWords = new LinHashTable<String>();
		words = new LinHashTable<String>();
		
		finalWords = new LinkedList<String>();
		positions = new LinkedList<String>();
		
		stackPos = new ArrayStack<String>(10000);
		charToRemove = new ArrayStack<String>(100);
		
		readFileWords("allWords.txt");
		readFilePreWords("allWords.txt");
		
		hashWords.print();
		
		solve();
		
	}
	
	public Boogle(int size){
		
		b = new Board();
		
		hashWords = new LinHashTable<String>(size);
		words = new LinHashTable<String>();
		
		finalWords = new LinkedList<String>();
		positions = new LinkedList<String>();
		
		stackPos = new ArrayStack<String>(10000);
		charToRemove = new ArrayStack<String>(100);
		
		readFileWords("allWords.txt");
		readFilePreWords("allWords.txt");
		
		//hashWords.print();
		
		solve();
		
		while(stackPos.empty() == false)
			System.out.println(stackPos.pop());
		
		positions.print();
		
	}
	
	public Boogle(int size, String pos){
		
		b = new Board();
		p = new Position(pos);
		
		hashWords = new LinHashTable<String>(size);
		words = new LinHashTable<String>();
		
		finalWords = new LinkedList<String>();
		positions = new LinkedList<String>();
		
		stackPos = new ArrayStack<String>(10000);
		charToRemove = new ArrayStack<String>(100);
		
		readFileWords("allWords.txt");
		readFilePreWords("allWords.txt");
		
		//hashWords.print();
		
		solve();
		
	}
	
	
	//Faz a leitura do ficheiro e guarda todas as palavras na hashtable
	public void readFileWords(String dir){
		
		String line;
		String file = dir;
		
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine();
			while (line  != null) {
				words.insere(line);
				line = br.readLine();
			}
			
		}catch(IOException e){ 
		      System.out.println("Ups");
		}
		
	}
	
	
	//Faz a leitura do ficheiro e guarda os prefixos de todas as palavras na hashtable
	public void readFilePreWords(String dir){

		String line;
		String file = dir;
		
		
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine();
			while (line  != null){
				String pre = "";
				for(int i = 0; i < line.length(); i++ ){
					pre = pre + line.charAt(i);
					hashWords.insere(pre);
				}
				line = br.readLine();
			}
			
		}catch(IOException e){ 
		      System.out.println("Ups");
		}
		
	}
	
	
	
	//Le todas as palavras possiveis a começar em pos
	public void lerBoogle(String pos){
		//Criar uma stack, ir guardando as posições na stack e vai avançando quando nao tiver mais posições possiveis vai buscar posições à stack
		// até nao haver mais possibilidades
		
		p = new Position(pos);		
		Node<String> a = p.availablePositions.header;
	
	
		pushStack(a);
		//stackPos.push(pos);
		
		while(stackPos.empty() == false){
			boolean v =true;
			while(v){
				
				if(checkWords(stackPos.top(), p) == true){
				
				//	positions.add(stackPos.top());
					
					String s = stackPos.top();
					//char c =  b.getChar(s);
					//s = c + "";
					charToRemove.push(s);
					
					
					//System.out.println(s + "::::::");
					p.move(stackPos.pop());
					
					
					a = p.availablePositions.header;
					
					//stackPos.push("!");
					pushStack(a);
					
					v = false;
				
				}else{
					
					if(stackPos.empty() == true)
						break;
					
					if(stackPos.top().contains("!") == true && charToRemove.empty() == false){
						/*
						Node<String> temp = p.Positions.header;
						String stg = "";
						while(temp != null){
							if(temp.elemento != null)
								stg = b.getChar(temp.elemento) + stg ;
							
							temp = temp.next;
						}
						
						if(hashWords.procurar(stg) != null){
							if(words.procurar(stg) != null){
								order(p);
								words.remove(stg);
							}
						}
						*/
						
						//System.out.println("Char to remove:" + charToRemove.top());
						//p.Positions.print();
						p.Positions.remove(charToRemove.pop());
						//p.Positions.print();
						stackPos.pop();
					
					}else{
						stackPos.pop();
				
					}					
					
				}
				
			}	
			
		}
		
	}
	

	
	public boolean checkWords(String pos, Position p){
		
		
		Node<String> bnode = p.Positions.header;
		
		char c;
		String s = "";
		
		while(bnode != null){
			
			if(bnode.elemento != null ){//&& bnode.next != null){
			//	System.out.println("!" + bnode.elemento);
				c = b.getChar(bnode.elemento);
				//System.out.println(b.getChar(bnode.elemento));
				s = c + s;
			}
			
			bnode = bnode.next;
			
		}
		
		//if(pos != null){//stackPos.empty() == false){
		s = s + b.getChar(pos);	
			//System.out.println(s);
			//System.out.println(b.getChar(pos));
		//}
		
		//System.out.println("!!" + pos);
		//System.out.println("!" + s);
		//System.out.println("!!" + pos);
		
		if(hashWords.procurar(s) != null){
			if(words.procurar(s) != null){
				order(p);
				words.remove(s);
				
			}
			return true;
		
		}else{
		//	s = s.substring(0, s.length() -1);
		}
		return false;
		
		
	}
	
	
	//usa o metodo lerBoogle para ler todas as palavras	
	public void solve(){
		
		
		
		
		for(int j = 0; j < 4; j++ ){
			for(int k = 0; k < 4; k++ ){
				lerBoogle("0,0");//j + "," + k);
			}
		}
		
		
		
		
	}
	
	
	//Pega numa palavra completa e adiciona a uma lista que vai ser o output do programa
	public void order(Position p){
		
		
		Node<String> z = p.Positions.header;
		
		String s = "";
		String st = "";
		
		while(z != null ){//&& z.next != null){
			
			if(z.elemento != null){

				s = b.getChar(z.elemento) + s;
				
				st =  " --> " + "(" + b.getChar(z.elemento) + ":(" + z.elemento +"))" + st ;
				
			}
			z = z.next;
			
		}

		s = s + b.getChar(stackPos.top());
		
		st =  st + " --> " + "(" + b.getChar(stackPos.top()) + ":(" + stackPos.top() +"))";
		
		st = s + st; 
		
		positions.add(st);
		
	}

	
	//Adiciona as posições disponiveis à stack
	public void pushStack(Node<String> a){
		
		while(a != null){
			if(a.elemento != null){
				//System.out.println("Pos:" + a.elemento);
				stackPos.push(a.elemento);	
			}
			a = a.next;
			
		}
			
	}
	
	

	
	public static void main(String[] args){
		
		Boogle bg = new Boogle(1);
		
	
		
		//System.out.println(bg.hashWords.procurar("seldo"));
		//System.out.println(bg.hashWords.procurar("se"));
		//System.out.println(bg.hashWords.procurar("sel"));
		//System.out.println(bg.hashWords.procurar("seu"));
		//System.out.println(bg.hashWords.procurar("sem"));
		//System.out.println(bg.hashWords.procurar("seo"));
		//System.out.println(bg.b.getChar(bg.p.currentPos));
		//System.out.println(bg.hashWords.arraySize);
		//System.out.println(bg.hashWords.counter);
		
		
		
	
	}
	
	
	
	

}
