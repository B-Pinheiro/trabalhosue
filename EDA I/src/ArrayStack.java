

public class ArrayStack<E> {

	private int x;
	E[] Stack;
	int top = -1;
	
	
	ArrayStack(int x){
		this.x = x;
		Stack = (E[]) new Object[x];
		
	}

	public void push(E o) {
		if(top+1 < x){
			top++;
			Stack[top] = o;
			
		}
	}


	public E top() {
		if(top > -1)
			return Stack[top];
		
		return Stack[top+1];
	}


	public E pop() {
		if(top > -1)
			top--;
				
		return Stack[top+1];
	
	}


	public int size() {
		return top+1; 
	}

	
	public boolean empty() {
		if(top == -1)
			return true;
		else{
			return false;
		}
	}

	
	public static void main(String[] args){
		
		ArrayStack<String> a = new ArrayStack<String>(100);
		
		a.push("s");
		a.push("se");
		a.push("sel");
		a.push("seo");
		a.push("sem");
		a.push("seu");
		a.push("sq");
		
		while(a.empty() == false){
			System.out.println(a.pop());
			
		//	System.out.println(a.top());
			
		}
		
		System.out.println("!");
		
	}
	
}
