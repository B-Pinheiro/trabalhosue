
public class Elemento<T> {
	
	T ele;

	Elemento(){
		ele = null;
	}
	
	Elemento(T x){
		ele = x;
	}
	
	
	public T getElemento(){
		return ele;
	}
	
	public void setElemento(T elemento){
		elemento = ele;
	}

}
