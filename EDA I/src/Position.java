import java.util.ArrayList;


public class Position {
	
	//Posições possiveis N, S, W, E, NE, SE, SW, NW
	// S = posx + 1
	// N = posx - 1
	// W = posy - 1
	// E = posy + 1
	// NE = posx - 1 + posy + 1
	// SE = posx + 1 + posy + 1 
	// SW = posx + 1 + posy - 1 
	// NW = posx - 1 + posy - 1 
	
	
	LinkedList<String> availablePositions;
	LinkedList<String> Positions;
	
	String currentPos;
	int posx;
	int posy;
	
	
	public Position(int posx, int posy){
		setPos(posx, posy);
		Positions  = new LinkedList<String>();
		Positions.add(currentPos);
		availablePosPrimeiraPos(currentPos);
		
	}
	
	public Position(String pos){
		currentPos = pos;
		Positions  = new LinkedList<String>();
		Positions.add(pos);
		availablePosPrimeiraPos(currentPos);
		
	}
	
	
	public void setPos(int x, int y){
		posx = x;
		posy = y;
		currentPos = posx + "," + posy; 
	}
	
	public void setPosition(String pos){
		currentPos = pos;
	}
	
	public int getPosx(){
		return posx;
	}
	
	public int getPosy(){
		return posy;
	}
	
	public String getPos(){
		return currentPos;
	}
	

	//Adiciona as posiçoes para onde é possivel andar
	public void availablePos(String pos){
	
		availablePositions  = new LinkedList<String>();
		
		switch(pos){
		
			case"0,0":
				availablePositions.add("0,1");
				availablePositions.add("1,0");
				availablePositions.add("1,1");
				break;
				
			case"0,1":
				availablePositions.add("0,0");
				availablePositions.add("0,2");
				availablePositions.add("1,0");
				availablePositions.add("1,1");
				availablePositions.add("1,2");
				break;
				
			case"0,2":
				availablePositions.add("0,1");
				availablePositions.add("0,3");
				availablePositions.add("1,1");
				availablePositions.add("1,2");
				availablePositions.add("1,3");
				break;
				
			case"0,3":
				availablePositions.add("0,2");
				availablePositions.add("1,2");
				availablePositions.add("1,3");
				break;
				
			case"1,0":
				availablePositions.add("0,0");
				availablePositions.add("2,0");
				availablePositions.add("0,1");
				availablePositions.add("1,1");
				availablePositions.add("2,1");
				break;
			
			case"1,1":
				availablePositions.add("0,0");
				availablePositions.add("0,1");
				availablePositions.add("0,2");
				availablePositions.add("1,0");
				availablePositions.add("1,2");
				availablePositions.add("2,0");
				availablePositions.add("2,1");
				availablePositions.add("2,2");
				break;
				
			case"1,2":
				availablePositions.add("0,1");
				availablePositions.add("0,2");
				availablePositions.add("0,3");
				availablePositions.add("1,1");
				availablePositions.add("1,3");
				availablePositions.add("2,1");
				availablePositions.add("2,2");
				availablePositions.add("2,3");
				break;
				
			case"1,3":
				availablePositions.add("0,2");
				availablePositions.add("0,3");
				availablePositions.add("1,2");
				availablePositions.add("2,2");
				availablePositions.add("2,3");
				break;
				
			case"2,0":
				availablePositions.add("1,0");
				availablePositions.add("1,1");
				availablePositions.add("2,1");
				availablePositions.add("3,0");
				availablePositions.add("3,1");
				break;
			
			case"2,1":
				availablePositions.add("1,0");
				availablePositions.add("1,1");
				availablePositions.add("1,2");
				availablePositions.add("2,0");				
				availablePositions.add("2,2");
				availablePositions.add("3,0");
				availablePositions.add("3,1");
				availablePositions.add("3,2");
				break;
			
			case"2,2":
				availablePositions.add("1,1");
				availablePositions.add("1,2");
				availablePositions.add("1,3");
				availablePositions.add("2,1");
				availablePositions.add("2,3");
				availablePositions.add("3,1");
				availablePositions.add("3,2");
				availablePositions.add("3,3");
				break;
			
			case"2,3":
				availablePositions.add("1,2");
				availablePositions.add("1,3");
				availablePositions.add("2,2");
				availablePositions.add("3,2");
				availablePositions.add("3,3");
				break;
				
			case"3,0":
				availablePositions.add("2,0");
				availablePositions.add("2,1");
				availablePositions.add("3,1");
				break;
				
			case"3,1":
				availablePositions.add("2,0");
				availablePositions.add("2,1");
				availablePositions.add("2,2");
				availablePositions.add("3,0");
				availablePositions.add("3,2");
				break;
			
			case"3,2":
				availablePositions.add("2,1");
				availablePositions.add("2,2");
				availablePositions.add("2,3");
				availablePositions.add("3,1");
				availablePositions.add("3,3");
				break;
				
			case"3,3":
				availablePositions.add("2,2");
				availablePositions.add("2,3");
				availablePositions.add("3,2");
				break;
				
			default:
				System.out.println("Posição Invalida!");
				break;
				
		}
		availablePositions.add("!");
		
	}
	
	
	//Adiciona as posiçoes para onde é possivel andar
		public void availablePosPrimeiraPos(String pos){
		
			availablePositions  = new LinkedList<String>();
			
			switch(pos){
			
				case"0,0":
					availablePositions.add("0,1");
					availablePositions.add("1,0");
					availablePositions.add("1,1");
					break;
					
				case"0,1":
					availablePositions.add("0,0");
					availablePositions.add("0,2");
					availablePositions.add("1,0");
					availablePositions.add("1,1");
					availablePositions.add("1,2");
					break;
					
				case"0,2":
					availablePositions.add("0,1");
					availablePositions.add("0,3");
					availablePositions.add("1,1");
					availablePositions.add("1,2");
					availablePositions.add("1,3");
					break;
					
				case"0,3":
					availablePositions.add("0,2");
					availablePositions.add("1,2");
					availablePositions.add("1,3");
					break;
					
				case"1,0":
					availablePositions.add("0,0");
					availablePositions.add("2,0");
					availablePositions.add("0,1");
					availablePositions.add("1,1");
					availablePositions.add("2,1");
					break;
				
				case"1,1":
					availablePositions.add("0,0");
					availablePositions.add("0,1");
					availablePositions.add("0,2");
					availablePositions.add("1,0");
					availablePositions.add("1,2");
					availablePositions.add("2,0");
					availablePositions.add("2,1");
					availablePositions.add("2,2");
					break;
					
				case"1,2":
					availablePositions.add("0,1");
					availablePositions.add("0,2");
					availablePositions.add("0,3");
					availablePositions.add("1,1");
					availablePositions.add("1,3");
					availablePositions.add("2,1");
					availablePositions.add("2,2");
					availablePositions.add("2,3");
					break;
					
				case"1,3":
					availablePositions.add("0,2");
					availablePositions.add("0,3");
					availablePositions.add("1,2");
					availablePositions.add("2,2");
					availablePositions.add("2,3");
					break;
					
				case"2,0":
					availablePositions.add("1,0");
					availablePositions.add("1,1");
					availablePositions.add("2,1");
					availablePositions.add("3,0");
					availablePositions.add("3,1");
					break;
				
				case"2,1":
					availablePositions.add("1,0");
					availablePositions.add("1,1");
					availablePositions.add("1,2");
					availablePositions.add("2,0");				
					availablePositions.add("2,2");
					availablePositions.add("3,0");
					availablePositions.add("3,1");
					availablePositions.add("3,2");
					break;
				
				case"2,2":
					availablePositions.add("1,1");
					availablePositions.add("1,2");
					availablePositions.add("1,3");
					availablePositions.add("2,1");
					availablePositions.add("2,3");
					availablePositions.add("3,1");
					availablePositions.add("3,2");
					availablePositions.add("3,3");
					break;
				
				case"2,3":
					availablePositions.add("1,2");
					availablePositions.add("1,3");
					availablePositions.add("2,2");
					availablePositions.add("3,2");
					availablePositions.add("3,3");
					break;
					
				case"3,0":
					availablePositions.add("2,0");
					availablePositions.add("2,1");
					availablePositions.add("3,1");
					break;
					
				case"3,1":
					availablePositions.add("2,0");
					availablePositions.add("2,1");
					availablePositions.add("2,2");
					availablePositions.add("3,0");
					availablePositions.add("3,2");
					break;
				
				case"3,2":
					availablePositions.add("2,1");
					availablePositions.add("2,2");
					availablePositions.add("2,3");
					availablePositions.add("3,1");
					availablePositions.add("3,3");
					break;
					
				case"3,3":
					availablePositions.add("2,2");
					availablePositions.add("2,3");
					availablePositions.add("3,2");
					break;
					
				default:
					System.out.println("Posição Invalida!");
					break;
					
			}
			
		}
	
	
	

	//"Anda" no tabuleiro, calculando as novas posições disponiveis para movimentar
	public void move(String position){
		
		if(availablePositions.contains(position) == true){
			
			Positions.add(position);
			
			availablePos(position);
			
			Node<String> a = Positions.header;
			
			while(a != null){
				if (availablePositions.contains(a.elemento) == true)
					availablePositions.remove(a.elemento);
				
				a = a.next;
				
			}
			
			
			currentPos = position;
							
		}else{
			//System.out.println("Não é possivel movimentar nesse sentido!");		
		}
				
	}
	
	
	public boolean equals(int x, int y){
		return (posx == x && posy == y);	
	}
	
	public boolean equals(String pos){
		return (currentPos == pos);
	}
	
	public String toString(){
		return "("+ currentPos + ")";
	}
	
	
	
}
