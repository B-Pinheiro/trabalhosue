import java.util.Arrays;


public abstract class HashTable<T> {
	
	T[] table;
	int arraySize;
	int counter = 0;
	
	public HashTable(){
		this(1);
	}
	
	public HashTable(int size){
		arraySize = size;		
		table = (T[]) new Object[size];
		
	}
	
	protected abstract  int procPos(T s);
	
	
	public int ocupados(){
		if (counter > 0)
			return counter;
		else
			System.out.println("Não existem elementos na HashTable");
			
		return (Integer) null;
	}
	
	public double factorCarga(){
		double fcarga = (double) counter/ (double) arraySize;
		//System.out.println(counter);
		//System.out.println(arraySize);
		//System.out.println(fcarga);
		
		return fcarga;	
		
	}
		
	
	public boolean precisaAlocar(){
	
		//if(counter > (arraySize / 2))
		if(factorCarga() > 0.6)
			return true;
		
		return false;
		
	}
	
	public void tornarVazia(){
		HashTable<T> ht = new LinHashTable<T>(arraySize);	
	}
	
	
	public int procHash(T s){
		int hashcode = s.hashCode();
		
		hashcode = Math.abs(hashcode % table.length);
		
		return hashcode;
		
	}
	
	public T procurar(T x){
		// encontrar
		int pos = procPos(x);
		
		if(table[pos] != null)
			if(table[pos].equals(x))
				return table[pos];
		
		return null;
		
	}
	
	public void remove(T x){
		
		int pos = procPos(x);
		
		if(table[pos] != null){
			if(table[pos].equals(x)){
				table[pos] = null;
				counter --;
			}
		}
		
	}

	
	public void insere(T x){
	
		if(procurar(x) != null){
			System.out.print("");
		}else{
			
			int arrayIndex = procPos(x);
	
			//System.out.println("Modulus Index = " + arrayIndex + " for value" + x);	
			table[arrayIndex] = x;
			counter ++;
		
		}
		
		if(precisaAlocar() == true)
			rehash();

		
	}
	
	
	public boolean isPrime(int n){
		if(n % 2 == 0)
			return false;
		
		for(int i = 3; i * i <= n; i += 2 ){
			
			if(n % i == 0)
				return false;
		}
		
		return true;
	}
	
	public int getNextPrime(int min){
		
		for(int i = min; true; i++){
			
			if(isPrime(i))
				return i;
		}
		
	}
	
	public int increaseArraySize(int newArraySize){
		arraySize = newArraySize;
		return arraySize;
	}
	
	
	public void rehash(){
		
		arraySize = getNextPrime(table.length * 2);
		
		HashTable<T> newHT = new LinHashTable<T>(arraySize);
		//newHT.counter = counter;
		
		for(int i = 0; i < table.length; i++){
			if(table[i] != null)
				//if((newHT.procurar(table[i])) == null)
					newHT.insere(table[i]);
		}
	
		table = newHT.table;
		arraySize = table.length;
		counter = newHT.counter;	
	
	}	
	
	
	public void print(){
		
		for(int i = 0; i < table.length; i++){
			if(table[i] != null){
				System.out.println("Index " +i + ": " + table[i]);
				
			}
		}
		
	}	
	
	
	
	

}
