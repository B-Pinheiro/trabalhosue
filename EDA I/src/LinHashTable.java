import java.util.Arrays;


public class LinHashTable<T> extends HashTable<T>{

	
	public LinHashTable(){
		super();
	}
	
	public LinHashTable(int size){
		super(size);
	}
	
	protected int procPos(T s) {
		
		int hashcode = s.hashCode();
		
		hashcode = Math.abs(hashcode % table.length);
		
		while(table[hashcode] != null){
			if(table[hashcode].equals(s))
				break;
			++hashcode;
			hashcode = Math.abs(hashcode % table.length);
		
		}
		
		return hashcode;
		
	}
	

	

}
