import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Board {

	char matrix[][];
	
	public Board(){
		matrix = new char[4][4];
		readFileMatrix("matrix.txt");	
	}
	
	// Faz a leitura do Ficheiro e adiciona as letras à matrix	
	public void readFileMatrix(String dir){
		
		String line;
		String file = dir;
		int counter = 0;
		char[] c;
		
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine();
			while (line  != null) {
				c = line.toCharArray();
				for(int i = 0; i < c.length; i++){
					matrix[counter][i] = c[i];
				}
				counter ++;
				line = br.readLine();
			}
			
		}catch(IOException e){ 
		      System.out.println("Ups");
		}
		
	}
	
	// Calcula a Letra numa determinada posição
	public char getChar(String pos){
		String temp;
		
		for(int i = 0; i < 4; i++){
			for(int n = 0; n < 4; n++){
				temp = i + "," + n;
				if(pos.equals(temp)){
					return matrix[i][n];
				}
			}
		}
		return ' ';
	}
	
	
	
	// Faz um print do Tabuleiro
	public void printBoard(){
		
		for(int j = 0; j < 4; j++){
			System.out.println();
			for(int k = 0; k < 4; k++){
				System.out.print(matrix[j][k]);
			}
		}	
		
	}
		

	
	
}
