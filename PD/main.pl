:-include('game.pl').
:-include('board.pl').
:-initialization(main).

main:-
    tabuleiro,
    play.

play:-
  write('Jogada: '),
  g(X),
  ( ( calcula(X, Y, Z),
      jogadapostal(Y, Z),
      write('Jogada Válida!!'),
      nl, tabuleiro, play )
  ; write('Jogada Inválida!!'), tabuleiro ).

tabuleiro:-
  nl, write('  Tabuleiro de Jogo'), nl,
  imptab.
