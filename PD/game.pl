/*Trata do input*/
g(S) :- get0(C), g([], C, S).
g(S, 11, S):- !.
g(S, 10, S):- !.
g(S, -1, S):- !.
g(I, C, [C|O]) :- get0(CC), g(I, CC, O).

/*Compara os numeros posiveis das posições do tabuleiro*/
compara(49):-!.
compara(50):-!.
compara(51):-!.
compara(52):-!.
compara(53):-!.
compara(54):-!.
compara(55):-!.
compara(56):-!.

/*Recebe dois chars e calcula a soma deles*/
calculaX(A, B, X):-
  Y is A-48,
  Z is B-48,
  X is (Y*10)+Z.

/*Verifica se o input é valido e calcula as posições*/
calcula([X1, X2, X3, X4|Xs], Y, Z):-
  compara(X1),
  compara(X2),
  compara(X3),
  compara(X4),
  calculaX(X1, X2, Y),
  calculaX(X3, X4, Z).

/*Se existe Y verifica se é uma jogada valida*/
jogadapostal(Y, Z):-
  peca(P, C, Y),
  okjogada(P, C, Y, Z), !.

/*Escolhe a verificação com base na peça*/
okjogada(p, C, Y, Z):- okpeao(C, Y, Z).
okjogada(t, C, Y, Z):- oktorre(C, Y, Z).
okjogada(c, C, Y, Z):- okcavalo(C, Y, Z).
okjogada(b, C, Y, Z):- okbispo(C, Y, Z).
okjogada(d, C, Y, Z):- okdama(C, Y, Z).
okjogada(r, C, Y, Z):- okrei(C, Y, Z).

/*Valida as Jogadas do Peão*/
okpeao(_, Y, Z) :- /*Movimento de uma casa*/
  \+ peca(_, _, Z),
  ( Z is Y+1; Z is Y-1 ),
  movimento(Y, Z), !.

okpeao(_, Y, Z) :- /*Movimento de duas casas*/
  \+ peca(_, _, Z),
  ( 2 is ((Y mod 10))
  ; 7 is ((Y mod 10)) ),
  ( Z is Y+2; Z is Y-2 ),
  movimento(Y, Z), !.

okpeao(b, Y, Z):- /*Movimento e captura (brancas)*/
  peca(_, p, Z),
  Y1 is Y-9,
  Y2 is Y+11,
  (Z is Y1; Z is Y2),
  movimentocapt(Y, Z), !.

okpeao(p, Y, Z):- /*Movimento e captura (pretas)*/
  peca(_, b, Z),
  Y1 is Y+9,
  Y2 is Y-11,
  (Z is Y1; Z is Y2),
  movimentocapt(Y, Z), !.

/*Valida as Jogadas da Torre*/
oktorre(C, Y, Z):-
  Z > Y, Z1 is Z-Y, Z1 > 8,
  okmovimento(10, Z1, C, Y, Z), !.

oktorre(C, Y, Z):-
  Z > Y, Z1 is Z-Y, Z1 < 8,
  okmovimento(1, Z1, C, Y, Z), !.

oktorre(C, Y, Z):-
  Y > Z, Y1 is Y-Z, Y1 > 8, Y3 is Z-Y, Y3 is Z-Y,
  okmovimento(-10, Y3, C, Y, Z), !.

oktorre(C, Y, Z):-
  Y > Z, Y1 is Y-Z, Y1 < 8, Y3 is Z-Y,
  okmovimento(-1, Y3, C, Y, Z), !.

/*Valida as Jogadas do Cavalo*/
okcavalo(_, Y, Z):-
  \+ peca(_, _, Z),
  ( Z is Y+8; Z is Y-8
  ; Z is Y+12; Z is Y-12
  ; Z is Y+19; Z is Y-19
  ; Z is Y+21; Z is Y-21),
  movimento(Y, Z), !.

okcavalo(b, Y, Z):-
  peca(_, p, Z),
  ( Z is Y+8; Z is Y-8
  ; Z is Y+12; Z is Y-12
  ; Z is Y+19; Z is Y-19
  ; Z is Y+21; Z is Y-21),
  movimentocapt(Y, Z), !.

okcavalo(p, Y, Z):-
  peca(_, b, Z),
  ( Z is Y+8; Z is Y-8
  ; Z is Y+12; Z is Y-12
  ; Z is Y+19; Z is Y-19
  ; Z is Y+21; Z is Y-21),
  movimentocapt(Y, Z), !.

/*Valida as Jogadas do Bispo*/
okbispo(C, Y, Z):-
  Z > Y, Z1 is Z-Y, Z2 is Z1 mod 11, Z2 is 0,
  okmovimento(11, Z1, C, Y, Z), !.

okbispo(C, Y, Z):-
  Z > Y, Z1 is Z-Y, Z2 is Z1 mod 9, Z2 is 0,
  okmovimento(9, Z1, C, Y, Z), !.

okbispo(C, Y, Z):-
  Y > Z, Y1 is Y-Z, Y2 is Y1 mod 11, Y2 is 0, Y3 is Z-Y,
  okmovimento(-11, Y3, C, Y, Z), !.

okbispo(C, Y, Z):-
  Y > Z, Y1 is Y-Z, Y2 is Y1 mod 9, Y2 is 0, Y3 is Z-Y,
  okmovimento(-9, Y3, C, Y, Z), !.

/*Valida as Jogadas da Dama*/
okdama(C, Y, Z):-
  okbispo(C, Y, Z), !.

okdama(C, Y, Z):-
  oktorre(C, Y, Z), !.

/*Valida as Jogadas do Rei*/
okrei(C, Y, Z):-
  \+ peca(_, _, Z),
  \+ existerei(C, Z),
  ( Z is Y+1; Z is Y-1
  ; Z is Y+9; Z is Y-9
  ; Z is Y+10; Z is Y-10
  ; Z is Y+11; Z is Y-11 ),
  movimento(Y, Z), !.

okrei(b, Y, Z):-
  peca(P, p, Z),
  okrei2(P, b, Y, Z).

okrei(p, Y, Z):-
  peca(P, b, Z),
  okrei2(P, p, Y, Z).

okrei2(P, C, Y, Z):-
  P \= 'r',
  \+ existerei(C, Z),
  ( Z is Y+1; Z is Y-1
  ; Z is Y+9; Z is Y-9
  ; Z is Y+10; Z is Y-10
  ; Z is Y+11; Z is Y-11 ),
  movimentocapt(Y, Z), !.

/* Sucede se existir um rei contrário em casas adjacentes*/
existerei(b, Z):-
  peca(r, p, Y),
  ( Z is Y+1; Z is Y-1
  ; Z is Y+9; Z is Y-9
  ; Z is Y+10; Z is Y-10
  ; Z is Y+11; Z is Y-11 ).

existerei(p, Z):-
  peca(r, b, Y),
  ( Z is Y+1; Z is Y-1
  ; Z is Y+9; Z is Y-9
  ; Z is Y+10; Z is Y-10
  ; Z is Y+11; Z is Y-11 ).


/*Movimento e captura Y -> Z se não tem casas pela frente*/
okmovimento(N, N, p, Y, Z):-
  peca(P, b, Z),
  P \= 'r',
  movimentocapt(Y, Z), !.

okmovimento(N, N, b, Y, Z):-
  peca(P, p, Z),
  P \= 'r',
  movimentocapt(Y, Z), !.

/*Movimento Y -> Z se não tem casas pela frente*/
okmovimento(N, N, _, Y, Z):-
  \+ peca(_, _, Z),
  movimento(Y, Z), !.

okmovimento(N, T, C, Y, Z):- /*Movimento de t casas*/
  Y1 is Y+T-N,
  \+ peca(_, _, Y1),
  T1 is T-N,
  okmovimento(N, T1, C, Y, Z).

movimento(Y, Z):-
  retract(peca(P, C, Y)),
  asserta(peca(P, C, Z)).

movimentocapt(Y, Z):-
  retract(peca(P, C, Y)),
  retract(peca(_, _, Z)),
  asserta(peca(P, C, Z)).

/*Imprime o Tabuleiro*/
imptab:-
  write('8 '), t(18, 8), write('7 '), t(17, 8),
  write('6 '), t(16, 8), write('5 '), t(15, 8),
  write('4 '), t(14, 8), write('3 '), t(13, 8),
  write('2 '), t(12, 8), write('1 '), t(11, 8), nl,
  write('   1 '), write('2 '), write('3 '), write('4 '),
  write('5 '), write('6 '), write('7 '), write('8 '), nl,nl.

t(_, 0):- nl, !.
t(X, N):-
  peca(P, C, X),
  ( C == 'b' ->( write(' '), lower_upper(P, P1), write(P1) );
  ( write(' '), write(P) ) ),
  X1 is X+10, N1 is N-1, t(X1, N1), !.

t(X, N):-
  \+ peca(_, _, X),
  write(' .'),
  X1 is X+10, N1 is N-1, t(X1, N1), !.
