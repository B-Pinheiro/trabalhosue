import java.util.ArrayList;


public class Tabuleiro {

	Pontuacao p;
	ArrayList<Ladrilho> tab;
	Ladrilho e1;
	Ladrilho e2;
	int n;
	
	boolean imp1;
	boolean imp2;
	
	
	
	Tabuleiro(){
		
		n = 4;
		tab = new ArrayList<Ladrilho> (4*4);
		p = new Pontuacao();
		
		
		for(int i = 0; i < 16; i++){
			tab.add(i, null);
		}
		
		e1 = new Ladrilho();
		e2 = new Ladrilho();
		
		tab.set(random(), e1);
		tab.set(random(), e2);
		
		
	}
	
	
	Tabuleiro(int n){
	
		if ( n >= 3){
			
		this.n = n;
		tab = new ArrayList<Ladrilho>(n*n);
		p = new Pontuacao();
		
		
		for(int i = 0; i < n*n; i++){
			tab.add(i, null);
		}
		
		e1 = new Ladrilho();
		e2 = new Ladrilho();
		
		tab.set(random(), e1);
		tab.set(random(), e2);
		
			
		}
		
		
	}
	
	
	
	
	@SuppressWarnings("null")
	int getN(int i, int j){				//Calcular o valor a partir do i e do j
		int temp;
		
		if(j <= n+1 && i <= n+1){
			temp = (n * (j-1)) + (i-1);
			return temp;
			
		}else{
			System.out.println("Não é possivel!");
			return (Integer) null;
		}
		
	}
	
	
	
	
	int random(){   //Calcular um numero random que esteja dentro do limite do tabuleiro
		int range = ((n*n)-1) + 1;
		return (int) (Math.random() * range); 
	}
	
	
	
	
	void cima(){ 
			
		movercima();
		imp2 = false;	
		
		for(int a = 1; a <= n; a++){  // a = i
			int counter =1;
			
			
			while(counter <= n  ){    // counter = j
				
				
				while(tab.get(getN(a, counter)) != null && tab.get(getN(a, counter+1)) != null){	//Verificar o se os Ladrilhos tem valor igual para poder somar
					

					if(tab.get(getN(a, counter)).valor == tab.get(getN(a, (counter + 1))).valor){  //Comparar o valores dos Ladrilhos
						
						imp2 = true;
						tab.set(getN(a,counter), new Ladrilho((tab.get(getN(a, counter)).valor)*2));   //Somar os Ladrilhos e remover o Ladrilho antigo
						tab.remove(getN(a, counter+1));
						tab.add(getN(a,counter+1), null);
						p.addScore(tab.get(getN(a, counter)).valor);						//Soma a pontuação
						movercima();															
	
					
					}else{
						counter++;		
					}
						
						
					if((counter+1) > n){       //Impedir que o programa continue fora do tabuleiro
						break;
					}
					
				}
				counter ++;
				
			}	
			
		}
		if(imp2 == true || imp1 == true){     //O jogo só faz update se puder mover ou somar algum ladrilho
			update();
		}
		
			
	}
	
	
	
	void baixo(){ 
			
			moverbaixo();
			imp2 = false;
			
			for(int a = 1; a <= n; a++){  // a = i
				int counter = n;
				
				while(counter >= 1  ){    // counter = j
					
					
					while(tab.get(getN(a, counter)) != null && tab.get(getN(a, counter - 1)) != null){	//Verificar o se os Ladrilhos tem valor igual para poder somar
						
						
						if(tab.get(getN(a, counter)).valor == tab.get(getN(a, (counter - 1))).valor){  //Comparar o valores dos Ladrilhos
						
							imp2 = true;
							tab.set(getN(a,counter), new Ladrilho((tab.get(getN(a, counter)).valor)*2));   //Somar os Ladrilhos e remover o Ladrilho antigo
							tab.remove(getN(a, counter - 1));
							tab.add(getN(a,counter - 1), null);
							p.addScore(tab.get(getN(a, counter)).valor);                 //SOma a pontuação
							moverbaixo();															
			
						}else{
							counter--;		
						}
								
						if((counter - 1) < 1){        //Impedir que o programa continue fora do tabuleiro
							break;
						}

					}
					counter --;
					
				}
			}
			if(imp2 == true || imp1 == true){    //O jogo só faz update se puder mover ou somar algum ladrilho
				update();
			}
			
	}
	
	
	
	
	void esq(){ 
		
		moveresq();
		imp2 = false;
				
		for(int a = 1; a <= n; a++){  // a = j
			int counter =1;
			
			while(counter <= n  ){    // counter = i
				
				
				while(tab.get(getN(counter, a)) != null && tab.get(getN(counter +1 , a)) != null){	//Verificar o se os Ladrilhos tem valor igual para poder somar
					

					if(tab.get(getN(counter, a)).valor == tab.get(getN(counter +1 , a)).valor){  //Comparar o valores dos Ladrilhos
					
						imp2 = true;
						tab.set(getN(counter, a), new Ladrilho((tab.get(getN(counter, a)).valor)*2));   //Somar os Ladrilhos e remover o Ladrilho antigo
						tab.remove(getN(counter +1, a));
						tab.add(getN(counter +1 , a), null);
						p.addScore(tab.get(getN(counter, a)).valor);                  		//Soma a pontuação
						moveresq();															
	
					
					}else{
						counter++;		
					}
						
						
					if((counter+1) > n){       //Impedir que o programa continue fora do tabuleiro
						break;
					}
					
				}
				counter ++;
				
			}	
		}
		
		if(imp2 == true || imp1 == true){   //O jogo só faz update se puder mover ou somar algum ladrilho
			update();
		}
		
		
	}
	
	
	
	
	void dir(){ 
		
		moverdir();
		imp2 = false;
		
		for(int a = 1; a <= n; a++){  // a = j
			int counter = n;
			
			while(counter >= 1  ){  // counter = i
				
				
				while(tab.get(getN(counter, a)) != null && tab.get(getN(counter -1, a)) != null){	//Verificar o se os Ladrilhos tem valor igual para poder somar
					
					
					if(tab.get(getN(counter, a)).valor == tab.get(getN(counter - 1, a)).valor){  //Comparar o valores dos Ladrilhos
					
						imp2 = true;
						tab.set(getN(counter, a), new Ladrilho((tab.get(getN(counter, a)).valor)*2));   //Somar os Ladrilhos e remover o Ladrilho antigo
						tab.remove(getN(counter -1, a));
						tab.add(getN(counter -1, a), null);
						p.addScore(tab.get(getN(counter, a)).valor);					//Soma a pontuação
						moverdir();															
		
					}else{
						counter--;		
					}
							
					if((counter - 1) < 1){     //Impedir que o programa continue fora do tabuleiro
						break;
					}

				}
				counter --;
				
			}
		}
		if(imp2 == true || imp1 == true){     //O jogo só faz update se puder mover ou somar algum ladrilho
			update();
		}
				
		
		
	}
	
	
	
	
	
	void add(Ladrilho x, int i, int j){  
		
		int temp;
	
		if(j <= n && i <= n){
			temp = (n * (j-1)) + (i-1);
			tab.set(temp, x);
			
		}else{
			System.out.println("Não é possivel adicionar!");
		}
		
		
	}
	
	
	
	void movercima(){ 
		
		imp1 = false;
		
		for(int a = 1; a <= n; a++){    //a = i
			int c = 1;		
			int counter = 1;			//counter = j
			
			
			while(counter <= n){ 
				
				while(tab.get(getN(a, counter)) == null){ // Verifica se existem espaços entre os Ladrilhos e se tiverem empurra-os para cima
					counter ++;
				
					if(counter > n){
						break;
					}
					
					if(tab.get(getN(a, counter)) != null){
						
						imp1 = true;
						tab.set(getN(a,c), tab.get(getN(a,counter)));
						tab.remove(getN(a, counter));
						tab.add(getN(a, counter), null);
						c++;
						
					}
				}
				
				if(counter > n){
					break;
				}else if(tab.get(getN(a, counter)) != null){ //Verificar se o algum elemento é != de null para fazer as incrementações
					counter ++;
					c++;
					
				}

			}	
			
		}	
		
	}
	
	
	
	void moverbaixo(){  
		
		imp1 = false;
		
		for(int a = 1; a <= n; a++){   //a = i
			int c = n; 
			int counter = n;			//counter = j
		
			
			while(counter >= 1){ 
				
				while(tab.get(getN(a, counter)) == null){ // Verifica se existem espaços entre os Ladrilhos e se tiverem empurra-os para baixo
					counter --; 
				
					if(counter < 1){ 
						break;
					}
					
					if(tab.get(getN(a, counter)) != null){
						
						imp1 = true;
						tab.set(getN(a,c), tab.get(getN(a,counter)));
						tab.remove(getN(a, counter));
						tab.add(getN(a, counter), null);
						c--;  
					
					}
					
				}
				
				if(counter < 1){
					break;
				}else if(tab.get(getN(a, counter)) != null){  //Verificar se o algum elemento é != de null para fazer as incrementações
					counter --; 
					c--;	
					
				}
				
			}	
			
		}	
		
		
	}
	
	
	
	void moveresq(){ 
		
		imp1 = false;
		
		for(int a = 1; a <= n; a++){ // a = j
			int c = 1;
			int counter = 1;	 //counter = i;
			
			
			while(counter <= n){ 
			
				while(tab.get(getN(counter, a)) == null){ // Verifica se existem espaços entre os Ladrilhos e se tiverem empurra-os para a esquerda
					counter ++;
				
					if(counter > n){
						break;
					}
					
					if(tab.get(getN(counter, a)) != null){
					
						imp1 = true;
						tab.set(getN(c, a), tab.get(getN(counter, a)));  
						tab.remove(getN(counter, a));
						tab.add(getN(counter, a), null);
						c++;
					
					}
					
				}
				if(counter > n){
					break;
				}else if(tab.get(getN(counter, a)) != null){
					counter ++;
					c++;
					
				}
				
			}	
				
		}	
		
		
	}


	void moverdir(){ 
		
		imp1 = false;
		
		for(int a = 1; a <= n; a++){ // a = j
			int c = n;
			int counter = n;	 //counter = i;
		
			
			while(counter >= 1){ 
			
				while(tab.get(getN(counter, a)) == null){ // Verifica se existem espaços entre os Ladrilhos e se tiverem empurra-os para a direita
					counter --;
				
					if(counter < 1){
						break;
					}
					
					if(tab.get(getN(counter, a)) != null){
					
						imp1 = true;
						tab.set(getN(c, a), tab.get(getN(counter, a)));  
						tab.remove(getN(counter, a));
						tab.add(getN(counter, a), null);
						c--;
					
					}
					
				}
				if(counter < 1){
					break;
				}else if(tab.get(getN(counter, a)) != null){
					counter --;
					c --;
					
				}
				
			}	
	
		}			
		
	}



	
	
	void update(){		// Mete um ladrilho novo num espaço random do tabuleiro
		
		Ladrilho e = new Ladrilho();
		boolean counter = true;
		
		
		while(counter){
			int temp = random();
			
			if(tab.get(temp) == null){
				tab.set(temp, e);
				counter = false;
				
			} 
		}	
		
	}
	
	
	
	boolean endgame(){			//Verifica se já nao existem espaços adicionais para acrescentar ladrilhos e acaba o jogo 
		boolean dead = false;
		int i = 0;
		
		while(tab.get(i) != null){
			
			if(i == (n*n)-1 ){
				dead = true;
				return dead;
			}
			i++;
		
		}
		return dead;
		
		
	}
	
	
	boolean vitoria(){			//Verifica se existe algum ladrilho com valor igual a 2048 
		
		boolean won = false;
		
		for(int i = 1; i < n; i++){
			for(int e = 1; e< n; e++){	
				
				if(tab.get(getN(e, i)) != null){
					
					if(tab.get(getN(e,i)).valor == 2048){
						won = true;
						return won;
					}
				}
			}
		}
		return won;

	}
	
	
	
	
	
	
	
}
