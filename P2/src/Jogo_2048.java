import java.util.Scanner;



public class Jogo_2048 {
	

	Tabuleiro t;
	Pontuacao p;
	
	
	Jogo_2048(){
		
		
		Scanner s = new Scanner(System.in);
		
		while(true){
			
			System.out.print("Insira o valor desejado para o tamanho do tabuleiro (nxn) : ");
			int n = s.nextInt();
			
			if(n >= 3){
			
				t = new Tabuleiro(n);
				p = new Pontuacao(); 
			
				
				p.loadScore();
				System.out.printf(" \n Recorde : %d ", p.highscore);
				System.out.println("\n");
			
				jogar();
				
			
				
			}else{
				
				System.out.println(" \n O tabuleiro tem tamanho minimo de 3x3");
				
			}
		
		}
		
			
	}
	
	
	
	void jogar(){ 
	
		mostrar();
		System.out.println("");
		
		System.out.printf("Pontuação : %d", t.p.getScore());
		System.out.println("\n");
		
		
		Scanner sc = new Scanner(System.in);
		
	
		while(true){
		
			
			//		1 - Cima
			//		2 - Baixo
			//		3 - Esquerda
			//		4 - Direita
			
			System.out.print("Insira o valor : ");
			
			int i = sc.nextInt();
			System.out.println("\n");
		
		 	switch(i){
		
			case 1:
				t.cima();	
				mostrar();
				
				System.out.println("Pontuação : " +  t.p.getScore());
				System.out.println("\n");
				break;	
			
			case 2:
				t.baixo();
				mostrar();
				
				
				System.out.println("Pontuação : " +  t.p.getScore());
				System.out.println("\n");
				break;	
			
			case 3:
				t.esq();
				mostrar();
				
				
				System.out.println("Pontuação : " +  t.p.getScore());
				System.out.println("\n");
				break;	
				
			case 4:
				t.dir();
				mostrar();
			
				
				System.out.println("Pontuação : " +  t.p.getScore());
				System.out.println("\n");
				break;	
			
		
				
			}
		
		 	
		 	
			if(t.vitoria() == true){
				System.out.println("VICTORIA");
				
				p.highScore(t.p.getScore());
				System.out.printf("Recorde : %d ", p.highscore);
				
				System.exit(0);
			}
			
		
			if(t.endgame() == true){
				System.out.println("DERROTA");
				
				p.highScore(t.p.getScore());
				System.out.printf("Recorde : %d ", p.highscore);
				
				System.exit(0);
			}
			
			
			
		}
		
		
		
	}
	
	
	
	void mostrar(){		
		int j = 1;
		
		for(int i = 0; i< t.n*t.n; i++){
			if(t.tab.get(i) != null){
				
				
				if(i == ((j*t.n)-1)){
					System.out.println(" " + t.tab.get(i).valor + " ");
					j++;
					
				}else{
					System.out.print(" " + t.tab.get(i).valor + " ");
				}
				

				
			}else{
				if(i == ((j*t.n)-1)){
					System.out.println(" - ");
					j++;
					
				}else{
					System.out.print(" - ");
				}
				
				
			}
			
		}
		
	}
		
	
	
	
	
	
}
