import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;


public class Pontuacao {
	
	int score;
	int highscore;
	FileReader f;
	Writer w;

	
	
	Pontuacao() {
		score = 0;	
	
	}
	

	void setScore(int s){
		score = s;
	}
	
	int getScore(){
		return score;
	}
	
	
	void addScore(int s){
		score += s;
	}
	
	
	void highScore(int s){ //faz load do highscore e compara, no caso de o score > highscore grava no ficheiro
		
		loadScore();
		
		if(s > highscore){
			highscore = s;
			System.out.println("Novo Recorde!!");
			saveScore();
		}
	
	
	}
	
	void saveScore(){
		
		try {
			w = new FileWriter("HighScore.txt");
			w.write(Integer.toString(highscore));
			w.close();
			
		} catch (IOException e) {}
				
	
	}
	
	
	
	void loadScore(){

		try {
			f = new FileReader("HighScore.txt");
			Scanner scanner = null;
			scanner = new Scanner(f); 
			
			
			while(scanner.hasNextInt()){
				
				highscore = Integer.parseInt(scanner.nextLine());
					
			}
			
			scanner.close();
		
			
		} catch (FileNotFoundException e) {}

		
	}
	
	void reset(){  //Faz o reset do highScore
		
		try {
			w = new FileWriter("HighScore.txt");
			w.write(Integer.toString(0));
			w.close();
		
		} catch (IOException e) {}
		
		loadScore();
			
	}

}
